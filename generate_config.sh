#!/bin/bash

echo "Generating JDBC Properties file"
sed -e "s;%HOST%;$1;g" -e "s;%DB%;$2;g" \
    -e "s;%USER%;$3;g" -e "s;%PASS%;$4;g" \
    templates/jdbc.properties.template > src/main/resources/jdbc.properties

echo "Generating Liquibase Properties file"
sed -e "s;%HOST%;$1;g" -e "s;%DB%;$2;g" \
    -e "s;%USER%;$3;g" -e "s;%PASS%;$4;g" \
    templates/liquibase.properties.template > liquibase/liquibase.properties

echo "Generating Login Properties file"
sed -e "s;%LOGIN_HOST%;$5;g" \
    templates/constants.properties.template > src/main/resources/constants.properties