#!/bin/bash -e

. /opt/bitnami/base/functions
. /opt/bitnami/base/helpers

print_welcome_page
check_for_updates &
if [[ "$1" == "nami" && "$2" == "start" ]] || [[ "$1" == "/init.sh" ]]; then
  while ! pg_isready -h postgres -p 5432 > /dev/null 2> /dev/null; do
    echo "Waiting 5 seconds till postgres is up"
    sleep 5
  done

  bash /liquibase/liquibase --defaultsFile=/liquibase/liquibase.properties --changeLogFile=/liquibase/changelog-main.xml --classpath=/liquibase/postgresql.jar update

  nami_initialize tomcat
  # SPIKE to make our app the root app ;)
  rm -Rf /bitnami/tomcat/data/ROOT
  info "Starting tomcat... "
fi

exec tini -- "$@"