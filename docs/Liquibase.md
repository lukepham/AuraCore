# Liquibase

#### Table of content
[Wiki](#wiki)

[Convention](#convention)

[Setup](#setup)

[Workflow](#workflow)


## Wiki
**Liquibase** is the tool for tracking, managing and applying DB schema changes. It is similar to Git, but getting control on DB state rather on code.
> Note: Liquibase is not the DB backup tool, in some cases (not always) it allows to restore previous DB state but not the data.

#### Keywords:
- [Changelog](http://www.liquibase.org/documentation/databasechangelog.html "Go to official docs") – the root of all Liquibase changes (.xml file).
- [Changeset](http://www.liquibase.org/documentation/changeset.html "Go to official docs") – declaration of database changes/refactorings (tagged block inside changelog).
- [Rollback](http://www.liquibase.org/documentation/rollback.html "Go to official docs") – the way to undo some changes (tagged block inside changeset for custom rollback).

Read [documentation](http://www.liquibase.org/documentation/) for more info.

## Convention
1.	Path to the all Liquibase resources (configuration, changelogs):
    ```
    {Project}\liquibase
    ```
2.	Main changelog only includes per-version changelogs files:
    ```xml
    <databaseChangeLog>
        ...
        <include file="changelog-v1.0.xml" relativeToChangelogFile="true"/>
        ...
    </databaseChangeLog>
     ```
3.	Version changelog includes all changesets that took place during current app’s version development:
    ```xml
    <databaseChangeLog>
        ...
        <changeSet>
            ...
        </changeSet>
        ...
    </databaseChangeLog>
     ```
4.	One changeset = one commit
5.	Fill in author and id attributes in <changeSet> tag correctly. Id here is kind of commit message, so give clear description what this changeset doing:
    ```xml
    <changeSet author="Author's name" id="Changeset's id">
    ```
6.	Use [Liquibase tags](http://www.liquibase.org/documentation/changes/index.html) for your <changeSet>. For complex query you can use pure sql or .sql file injection. But remember 4th rule of this list.
7.	If your changeset brings some major change to DB you should leave special tag <tagDatabase> there. It will be a special breakpoint which allows to rollback directly to it anytime in future.
    ```xml
    <changeSet author="userId" id="tag_version_0_1_0">
        <tagDatabase tag="version_0.1.0" />
    </changeSet>
    ```
8.	Don’t forget about <rollback> section in your <changeSet>. Even if Liquibase provides autorollback for some operation your custom rollback will override it.

## Setup
1.	Add Liquibase dependency to Maven project
2.	Setup [Liquibase Maven plugin](http://www.liquibase.org/documentation/maven/index.html)
-	Path to the property file with DB connection configuration
    ```xml
     <propertyFile>liquibase/liquibase.properties</propertyFile>
     ```
-	Path to the main changelog file (entry point to the changelog hierarchy)
    ```xml
     <propertyFile>liquibase/changelog-main.xml</propertyFile>
     ```
- Main execution (allows to skip Liquibase execution in Maven lifecycle till Package phase):
    ```xml
     <execution>
        <phase>package</phase>
        <goals>
           <goal>update</goal>
        </goals>
     </execution>
    ```
3.  Add Liquibase-Hibernate extension dependency to plugin dependencies
4.  Add path to generated changelog files (these files will not be commited);
    ```xml
            <diffChangeLogFile>liquibase/changelogs/diff-generatedchangelog-${maven.build.timestamp}.xml</diffChangeLogFile>
    ```
> Note: After Liquibase’s version update test its functionality. If plugin got problems with operating changelog files, you probably need to update changelog schema version as well.

## Workflow
1.	Add your changeset to the current version changelog:
-   You can write it manually
-   You can generate it automatically by comparing your database with models, annotated with JPA by running the following Maven command:
    ```
    mvn liquibase:diff
    ```
    This command will generate a new xml file with changesets that need to be applied to the database for it to correspond to models.
    Be sure to look through all the changesets and modify them if necessary, automatic changeset generation can contain errors and cause data loss.
    Things you should pay attention to:
        - data types;
        - data migration (especially when column/table is renamed);
        - rollbacks.
    Rollback tags must be added, as well as commands for data migration.
    Once everything is checked and edited, append these changesets to appropriate changelog file.
2.	Update DB using Maven command:
    ```
    mvn liquibase:update
    ```
3.	Check the results
4.	If you need rollback use one of further Maven command:
    ```
    mvn liquibase:rollback -Dliquibase.rollbackCount=1
    mvn liquibase:tag -Dliquibase.tag=checkpoint
    ```
> Note: There are few rollback modes. Pay attention to their arguments and always be careful with rollbacks to avoid damage to DB and data lose.