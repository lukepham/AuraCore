# Security

Project use Spring Security combined with JWT and OAuth frameworks.

#### Table of content
[Wiki](#wiki)

[Basic setting](#Basic_setting)

## Wiki
**Spring Security** is a framework that focuses on providing both authentication and authorization to Java applications. Like all Spring projects, the real power of Spring Security is found in how easily it can be extended to meet custom requirements.

**OAuth** is an open standard for access delegation, commonly used as a way for Internet users to grant websites or applications access to their information on other websites but without giving them the passwords.

**JWT** is JSON Web Token which a JSON-based open standard ([RFC 7519](https://tools.ietf.org/html/rfc7519 "Go to official docs")) for creating access tokens that assert some number of claims.

## Basic setting
Spring Security:
- [SecurityContextHolder](http://docs.spring.io/spring-security/site/docs/4.0.4.RELEASE/apidocs/org/springframework/security/core/context/SecurityContextHolder.html "Go to official docs") – This class provides a series of static methods that delegate to an instance of SecurityContextHolderStrategy. The purpose of the class is to provide a convenient way to specify the strategy that should be used for a given JVM. This is a JVM-wide setting, since everything in this class is static to facilitate ease of use in calling code.
- [SecurityContext](https://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/core/context/SecurityContext.html "Go to official docs") – Interface defining the minimum security information associated with the current thread of execution.
- Authentication – represents the user (Principal) from the point of view of Spring Security.
- [GrantedAuthority](http://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/core/GrantedAuthority.html "Go to official docs") – Represents an authority granted to an Authentication object.
A GrantedAuthority must either represent itself as a String or be specifically supported by an AccessDecisionManager.
- UserDetails - provides the necessary information to construct an Authentication object from DAO application objects or other security data sources.

OAuth:
Oracle Access Management provides a graphical user interface for configuring OAuth Services. This chapter describes how to use the Oracle Access Management Console to configure OAuth Services and contains the following topics.
- [Enabling OAuth Services](https://docs.oracle.com/cd/E40329_01/admin.1112/e27239/oicconfigoauth.htm#CHDJHGDA "Go to official docs") – The standard OAuth Service is enabled if the Oracle Access Management Identity Federation service is enabled in Available Services. To also enable Mobile OAuth, enable the Mobile and Social service in addition to the Identity Federation service.
- [Opening the OAuth Services Configuration Page](https://docs.oracle.com/cd/E40329_01/admin.1112/e27239/oicconfigoauth.htm#BABBIAJH "Go to official docs") – Follow these steps to open OAuth Services configuration page in the Oracle Access Management Console.
    1. Log in to the Oracle Access Management Console. The Launch Pad opens.
    2. Click OAuth Service in the Mobile and Social pane. The OAuth Identity Domains page opens in its own tab.
- [Understanding OAuth Services Configuration](https://docs.oracle.com/cd/E40329_01/admin.1112/e27239/oicconfigoauth.htm#BABHDDIC") – The OAuth Identity Domains page lists all of the OAuth Identity Domains on the OAM Server. Click a domain to configure it. The following sections contain information about the tabs used to configure OAuth Services.

> Note: OAuth Services can be configured from the command line using WLST. For more information about the Mobile and Social WLST commands, see the 

Read [documentation](https://docs.oracle.com/cd/E40329_01/admin.1112/e27239/oicconfigoauth.htm#AIAAG89127) for more info.

JWT:
- Secret Key – `secret`. The key that will be used to sign your tokens. I decided to keep this separate from the Laravel APP_KEY so that developers can change them independently from each other.
- Token time to live - `ttl`. This is the length of time, in minutes, that your token will be considered valid. It is recommended that this is kept as short as possible, especially if utilising token refreshing.
- Refresh time to live - `refresh_ttl`. This is the length of time, in minutes, that you can refresh a token within. For example, if you set this to 2 weeks, then you will only be able to refresh the same chain of tokens for a maximum of 2 weeks before the token will be 'un-refreshable' and the result will always be a `TokenExpiredException`. So after this time has passed, a brand new token must be generated, and usually that means the user has to login again.
- Hashing algorithm - `algo`. This is the algorithm used to sign the tokens. Feel free to leave this as default.
- User model path - `user`. This should be the namespace path that points to your User class.
- User identifier - `identifier`. This is used for retreiving the user from the token subject claim.
- Required claims - `required_claims`. These claims must be present in the token payload or a `TokenInvalidException` will be thrown.
- Blacklist enabled - `blacklist_enabled`. If this option is set to false, then you will not be able to invalidate tokens. Although, you may still refresh tokens - the previous token will not be invalidated, so this is not the most secure option. Very simple implementations may not need the extra overhead, so that is why it is configurable.
- Providers. These are the concrete implementations that the package will use to achieve various tasks. You can override these, as long as the implementation adheres to the relevant interfaces.
- User - `providers.user`. Specify the implementation that is used to find the user based on the subject claim.
- JWT - `providers.jwt`. This will do the heavy lifting of encoding and decoding of the tokens.
- Authentication - `providers.auth`. This will retrieve the authenticated user, via credentials or by an id.
- Storage - `providers.storage`. This is used to drive the Blacklist, and store the tokens until they expire.

Read [documentation](https://jwt.io/) for more info.