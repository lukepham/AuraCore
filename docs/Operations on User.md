# Operations on "User" entity.

Operations available for user are described here.  

#### Table of content
1. [Add/Create User](#1.Add/Create User)
1.1. [User validation](#1.1.User validation)
1.2. [Security config for create User event](#1.2.Security config for create User event)



## 1.Add/Create User
Creation of user is provided by **(host)\api\users** endpoint:
+ At first you need to form a valid `xml` or `json` body for request. You **MUST** use a `UserWithCredentialsDto` model. 
For example (also you may find them at api documentation at `*yourhost*/api/swagger-ui.html#!/User/addUserUsingPOST`):
```json
{
  "contacts": {
    "addresses": [
      {
        "fullAddress": "string1234"
      }
    ],
    "emails": [
      {
        "emailAddress": "string1234"
      }
    ],
    "phones": [
      {
        "phoneNumber": "string"
      }
    ]
  },
  "department_id": 1,
  "firstName": "string",
  "isActive": true,
   "login": "login",
  "password": "password",
  "lastName": "string",
  "role": "USER"
}
```
+ Send body with POST method. 
+ According to previous example you will obtain a **new** User record assigned to _department #1_ with **new** Contacts,Email,Phone,Address, unless you have provided User with existing login or not valid input according to validation rules.

### 1.1.User validation

User validation constraints are provided at level of DTO and are described in yaml file for Swagger.
```yaml
UserWithCredentials:
    allOf:
      - $ref: '#/definitions/User'
      - type: object
        required:
          - firstName
          - lastName
          - contacts
          - isActive
        properties:
          login:
            type: string
          password:
            type: string
          role:
            type: string
            description: User athorization level
            enum:
            - ADMIN
            - MANAGER
            - USER
    xml:
      name: UserWithCredentials
```
According to code you must provide _Contacts_, _First/Last Name_, _Status of account "isActive"_.
Also if you provide User with existing login you will get an `AuraUserVAlidationException` that will be handled by Spring, so enduser will obtain JSON with HTTP status code, and simple description from message of Exception:
```json
{
    "code": 400,
    "message": "User validation error",
    "problems": []
}
```
or in xml:
```xml
<Error>
    <code>400</code>
    <message>User validation error</message>
</Error>
```

### 1.2.Security config for create User event
Only admin is allowed to POST at **(host)\api\users**. That constraint is provided in **SecurityConfig class**:
```java
.antMatchers(HttpMethod.POST, "/api/users").hasRole(ADMIN)
```
So you MUST login as Admin at `*yourhost*/api/login` before proceeding with creation of User

