package academy.softserve.aura.core.entity;

import java.math.BigDecimal;

public class ItemAmortizationStatistic {

    private BigDecimal currentPrice;
    private Long monthOfUse;
    private BigDecimal valueOfMonthAmortization;

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Long getMonthOfUse() {
        return monthOfUse;
    }

    public void setMonthOfUse(Long monthOfUse) {
        this.monthOfUse = monthOfUse;
    }

    public BigDecimal getValueOfMonthAmortization() {
        return valueOfMonthAmortization;
    }

    public void setValueOfMonthAmortization(BigDecimal valueOfMonthAmortization) {
        this.valueOfMonthAmortization = valueOfMonthAmortization;
    }
}
