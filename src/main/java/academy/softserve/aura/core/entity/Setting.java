package academy.softserve.aura.core.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "setting")
public class Setting {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "is_active", nullable = false)
    private boolean isActive;


    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="topic_names", joinColumns=@JoinColumn(name="setting_id"))
    @Column(name = "topic_name", nullable = false)
    private List<String> kafkaTopicNames;

    @Column(name = "microservice_host", nullable = false)
    private String microserviceHost;

    @Column(name = "kafka_host", nullable = false)
    private String kafkaHost;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<String> getKafkaTopicNames() {
        return kafkaTopicNames;
    }

    public void setKafkaTopicNames(List<String> kafkaTopicName) {
        this.kafkaTopicNames = kafkaTopicName;
    }

    public String getMicroserviceHost() {
        return microserviceHost;
    }

    public void setMicroserviceHost(String microserviceHost) {
        this.microserviceHost = microserviceHost;
    }

    public String getKafkaHost() {
        return kafkaHost;
    }

    public void setKafkaHost(String kafkaHost) {
        this.kafkaHost = kafkaHost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}


