package academy.softserve.aura.core.entity;

import java.math.BigDecimal;

public class ItemAmortization {
    private Long id;
    private boolean isWorking;
    private String manufacturer;
    private String model;
    private BigDecimal startPrice;
    private BigDecimal currentPrice;
    private Long monthOfUse;
    private String firstMonth;
    private String lastMonth;
    private BigDecimal valueOfAmortizationValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isWorking() {
        return isWorking;
    }

    public void setIsWorking(boolean working) {
        isWorking = working;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Long getMonthsOfUse() {
        return monthOfUse;
    }

    public void setMonthOfUse(Long monthOfUse) {
        this.monthOfUse = monthOfUse;
    }

    public String getFirstMonth() {
        return firstMonth;
    }

    public void setFirstMonth(String firstMonth) {
        this.firstMonth = firstMonth;
    }

    public String getLastMonth() {
        return lastMonth;
    }

    public void setLastMonth(String lastMonth) {
        this.lastMonth = lastMonth;
    }

    public BigDecimal getValueOfMonthAmortization() {
        return valueOfAmortizationValue;
    }

    public void setValueOfMonthAmortization(BigDecimal valueOfAmortizationValue) {
        this.valueOfAmortizationValue = valueOfAmortizationValue;
    }
}