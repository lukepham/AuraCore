package academy.softserve.aura.core.entity;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "item_log")
public class ItemLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "item_id", nullable = false)
    private String itemId;

    @Column(name = "event_date", nullable = false)
    private OffsetDateTime eventDate;

    @Enumerated
    @Column(name = "item_event_type")
    private ItemEventType eventType;

    @Column(name = "description", nullable = false)
    private String description;

    @Transient
    private Item item;

    public ItemLog() {
    }

    public ItemLog(String itemId, OffsetDateTime eventDate, ItemEventType eventType, String description) {
        this.itemId = itemId;
        this.eventDate = eventDate;
        this.eventType = eventType;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public OffsetDateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(OffsetDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public ItemEventType getEventType() {
        return eventType;
    }

    public void setEventType(ItemEventType eventType) {
        this.eventType = eventType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
