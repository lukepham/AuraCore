package academy.softserve.aura.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "department")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "department")
    private List<User> workers;

    @JsonIgnore
    @OneToMany(mappedBy = "department")
    private List<CommonItem> commonItems;


    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "contacts_id", referencedColumnName = "id", nullable = false)
    private Contacts contacts;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "common_item_department_owners",
            joinColumns = @JoinColumn(name = "department_id"),
            inverseJoinColumns = @JoinColumn(name = "common_item_item_id")
    )
    private List<CommonItem> allCommonItemsBelongToDepartment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getWorkers() {
        return workers;
    }

    public void setWorkers(List<User> workers) {
        this.workers = workers;
    }

    public List<CommonItem> getCommonItems() {
        return commonItems;
    }

    public void setCommonItems(List<CommonItem> commonItems) {
        this.commonItems = commonItems;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }

    public List<CommonItem> getAllCommonItemsBelongToDepartment() {
        return allCommonItemsBelongToDepartment;
    }

    public void setAllCommonItemsBelongToDepartment(List<CommonItem> allCommonItemsBelongToDepartment) {
        this.allCommonItemsBelongToDepartment = allCommonItemsBelongToDepartment;
    }
}