package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.ItemAmortization;
import academy.softserve.aura.core.swagger.model.ItemAmortizationDto;
import org.springframework.stereotype.Component;

@Component
public class ItemAmortizationMapper implements DtoMapper<ItemAmortizationDto, ItemAmortization>{

    @Override
    public ItemAmortizationDto toDto(ItemAmortization entity) {

        ItemAmortizationDto itemAmortizationDto =  new ItemAmortizationDto();

        if (entity == null){
            return null;
        }

        itemAmortizationDto.setId(entity.getId());
        itemAmortizationDto.setIsWorking(entity.isWorking());
        itemAmortizationDto.setManufacturer(entity.getManufacturer());
        itemAmortizationDto.setModel(entity.getModel());
        itemAmortizationDto.setStartPrice(entity.getStartPrice());
        itemAmortizationDto.setCurrentPrice(entity.getCurrentPrice());
        itemAmortizationDto.setMonthsOfUse(entity.getMonthsOfUse());
        itemAmortizationDto.setFirstMonth(entity.getFirstMonth());
        itemAmortizationDto.setLastMonth(entity.getLastMonth());
        itemAmortizationDto.setValueOfMonthAmortization(entity.getValueOfMonthAmortization());

        return itemAmortizationDto;
    }

    @Override
    public ItemAmortization toEntity(ItemAmortizationDto dto) {
        ItemAmortization itemAmortization = new ItemAmortization();

        if (dto == null){
            return null;
        }

        itemAmortization.setId(dto.getId());
        itemAmortization.setIsWorking(dto.getIsWorking());
        itemAmortization.setManufacturer(dto.getManufacturer());
        itemAmortization.setModel(dto.getModel());
        itemAmortization.setStartPrice(dto.getStartPrice());
        itemAmortization.setCurrentPrice(dto.getCurrentPrice());
        itemAmortization.setMonthOfUse(dto.getMonthsOfUse());
        itemAmortization.setFirstMonth(dto.getFirstMonth());
        itemAmortization.setLastMonth(dto.getLastMonth());
        itemAmortization.setValueOfMonthAmortization(dto.getValueOfMonthAmortization());

        return itemAmortization;
    }
}