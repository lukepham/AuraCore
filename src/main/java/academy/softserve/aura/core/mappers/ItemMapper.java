package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.swagger.model.ItemDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The mapper class to perform mapping between
 * {@link Item} entities and {@link ItemDto} DTO objects.
 * It implements basic {@link DtoMapper} and uses {@link ListMapper}.
 *
 * @see Item
 */
@Component
public class ItemMapper implements DtoMapper<ItemDto, Item> {

    private ListMapper<ItemDto, Item> itemListMapper;

    /**
     * Constructor
     * Initializes ListMapper
     */
    public ItemMapper() {
        itemListMapper = new ListMapper<ItemDto, Item>(null) {

            @Override
            List<ItemDto> toDtoList(List<Item> entityList) {
                if (entityList != null && !entityList.isEmpty()) {
                    ItemMapper itemMapper = new ItemMapper();
                    List<ItemDto> dtoList = new ArrayList<>(entityList.size());
                    dtoList.addAll(entityList.stream().map(itemMapper::toDto).collect(Collectors.toList()));
                    return dtoList;
                } else {
                    return new ArrayList<>();
                }
            }

            @Override
            List<Item> toEntityList(List<ItemDto> dtoList) {
                if (dtoList != null && !dtoList.isEmpty()) {
                    ItemMapper itemMapper = new ItemMapper();
                    List<Item> entityList = new ArrayList<>(dtoList.size());
                    entityList.addAll(dtoList.stream().map(itemMapper::toEntity).collect(Collectors.toList()));
                    return entityList;
                } else {
                    return new ArrayList<>();
                }
            }
        };
    }


    /**
     * Gets {@link Item} entity and maps them to specific DTO: {@link ItemDto}.
     *
     * @param entity the the entity {@link Item} type to be mapped to DTO object
     * @return mapped DTO object of type {@link ItemDto}
     */
    @Override
    public ItemDto toDto(Item entity) {
        if (entity == null) {
            return null;
        }

        ItemDto itemDto = new ItemDto();

        itemDto.setIsWorking(entity.isWorking());
        itemDto.setId(entity.getId());
        itemDto.setManufacturer(entity.getManufacturer());
        itemDto.setModel(entity.getModel());
        itemDto.setStartPrice(entity.getStartPrice());
        itemDto.setCurrentPrice(entity.getCurrentPrice());
        itemDto.setNominalResource(entity.getNominalResource());
        itemDto.setDescription(entity.getDescription());
        itemDto.setComponents(itemListMapper.toDtoList(entity.getComponents()));
        itemDto.setItemType(entity.getClass().getSimpleName());
        itemDto.setManufactureDate(entity.getManufactureDate());
        itemDto.setAgingFactor(entity.getAgingFactor());

        return itemDto;
    }

    /**
     * Gets {@link ItemDto} DTO object and maps them to specific entity: {@link Item}.
     *
     * @param dto the DTO {@link ItemDto} type to be mapped to DTO object
     * @return new entity object of type {@link Item}
     */
    @Override
    public Item toEntity(ItemDto dto) {
        if (dto == null) {
            return null;
        }

        Item item = new Item();

        if (dto.getIsWorking() != null) {
            item.setWorking(dto.getIsWorking());
        }

        item.setId(dto.getId());
        item.setManufacturer(dto.getManufacturer());
        item.setModel(dto.getModel());
        item.setDescription(dto.getDescription());
        item.setStartPrice(dto.getStartPrice());
        item.setCurrentPrice(dto.getCurrentPrice());
        item.setComponents(itemListMapper.toEntityList(dto.getComponents()));

        if (!item.getComponents().isEmpty()) {
            item.getComponents().forEach((u -> u.setParentItem(item)));
        }

        if (dto.getNominalResource() != null) {
            item.setNominalResource(dto.getNominalResource());
        }
        item.setManufactureDate(dto.getManufactureDate());
        item.setAgingFactor(dto.getAgingFactor());

        return item;
    }
}
