package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Email;
import academy.softserve.aura.core.swagger.model.EmailDto;
import org.springframework.stereotype.Component;

/**
 * The mapper class to perform mapping between
 * {@link Email} entities and {@link EmailDto} DTO objects.
 * It implements basic {@link DtoMapper}.
 *
 * @see Email
 */
@Component
public class EmailMapper implements DtoMapper<EmailDto, Email> {

    /**
     * Gets {@link Email} entity and maps them to specific DTO: {@link EmailDto}.
     *
     * @param entity the the entity {@link Email} type to be mapped to DTO object
     * @return mapped DTO object of type {@link EmailDto}
     */
    @Override
    public EmailDto toDto(Email entity) {
        if (entity == null) {
            return null;
        }

        EmailDto emailDto = new EmailDto();

        emailDto.setId(entity.getId());
        emailDto.setEmailAddress(entity.getEmailAddress());

        return emailDto;
    }
    
    /**
     * Gets {@link EmailDto} DTO object and maps them to specific entity: {@link Email}.
     *
     * @param dto the DTO {@link EmailDto} type to be mapped to DTO object
     * @return new entity object of type {@link Email}
     */
    @Override
    public Email toEntity(EmailDto dto) {
        if (dto == null) {
            return null;
        }

        Email email = new Email();

        email.setId(dto.getId());
        email.setEmailAddress(dto.getEmailAddress());

        return email;
    }
}
