package academy.softserve.aura.core.mappers;

import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
/**
 * The basic interface for Mapping operations.
 *
 * @param <E>  the type of Entity
 * @param <D> the type of corresponding DTO object
 */
@Validated
public interface DtoMapper<D, E> {

    /**
     * Maps Entity to corresponding DTO
     * @param entity the entity to be mapped to DTO object
     * @return mapped DTO object
     */
    D toDto(E entity);

    /**
     * Maps DTO to corresponding Entity
     * @param dto the DTO object that must be mapped to entity
     * @return mapped entity object
     */
    E toEntity(@Valid D dto);
}
