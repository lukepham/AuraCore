package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.swagger.model.DepartmentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The mapper class to perform mapping between
 * {@link Department} entities and {@link DepartmentDto} DTO objects.
 * It implements basic {@link DtoMapper}.
 * Uses {@link ContactsMapper}
 *
 * @see Department
 */
@Component
public class DepartmentMapper implements DtoMapper<DepartmentDto, Department> {

    private final ContactsMapper contactsMapper;

    @Autowired
    public DepartmentMapper(ContactsMapper contactsMapper) {
        this.contactsMapper = contactsMapper;
    }

    /**
     * Gets {@link Department} entity and maps them to specific DTO: {@link DepartmentDto}.
     *
     * @param entity the the entity {@link Department} type to be mapped to DTO object
     * @return mapped DTO object of type {@link DepartmentDto}
     */
    @Override
    public DepartmentDto toDto(Department entity) {
        if (entity == null) {
            return null;
        }

        DepartmentDto departmentDto = new DepartmentDto();

        departmentDto.setId(entity.getId());
        departmentDto.setName(entity.getName());
        departmentDto.setContacts(contactsMapper.toDto(entity.getContacts()));

        return departmentDto;
    }
    
    /**
     * Gets {@link DepartmentDto} DTO object and maps them to specific entity: {@link Department}.
     *
     * @param dto the DTO {@link DepartmentDto} type to be mapped to DTO object
     * @return new entity object of type {@link Department}
     */
    @Override
    public Department toEntity(DepartmentDto dto) {
        if (dto == null) {
            return null;
        }

        Department department = new Department();

        department.setId(dto.getId());
        department.setName(dto.getName());
        department.setContacts(contactsMapper.toEntity(dto.getContacts()));

        return department;
    }
}