package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Phone;
import academy.softserve.aura.core.swagger.model.PhoneDto;
import org.springframework.stereotype.Component;

/**
 * The mapper class to perform mapping between
 * {@link Phone} entities and {@link PhoneDto} DTO objects.
 * It implements basic {@link DtoMapper}.
 *
 * @see Phone
 */
@Component
public class PhoneMapper implements DtoMapper<PhoneDto, Phone> {

    /**
     * Gets {@link Phone} entity and maps them to specific DTO: {@link PhoneDto}.
     *
     * @param entity the the entity {@link Phone} type to be mapped to DTO object
     * @return mapped DTO object of type {@link PhoneDto}
     */
    @Override
    public PhoneDto toDto(Phone entity) {
        if (entity == null) {
            return null;
        }

        PhoneDto phoneDto = new PhoneDto();

        phoneDto.setId(entity.getId());
        phoneDto.setPhoneNumber(entity.getPhoneNumber());

        return phoneDto;
    }

    /**
     * Gets {@link PhoneDto} DTO object and maps them to specific entity: {@link Phone}.
     *
     * @param dto the DTO {@link PhoneDto} type to be mapped to DTO object
     * @return new entity object of type {@link Phone}
     */
    @Override
    public Phone toEntity(PhoneDto dto) {
        if (dto == null) {
            return null;
        }

        Phone phone = new Phone();

        phone.setId(dto.getId());
        phone.setPhoneNumber(dto.getPhoneNumber());

        return phone;
    }
}
