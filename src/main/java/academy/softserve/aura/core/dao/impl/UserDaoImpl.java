package academy.softserve.aura.core.dao.impl;

import academy.softserve.aura.core.dao.UserDao;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Repository
public class UserDaoImpl extends CrudDaoImpl<User> implements UserDao {

    private static final String USER_ROLE = "userRole";
    private static final String DEPARTMENT = "department";
    private static final String ID = "id";
    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public UserDaoImpl() {
        super(User.class);
    }

    @Override
    public Collection<User> findByRoles(int offset,
                                        int limit,
                                        Collection<UserRole> roles) {

        Session session;
        Collection<User> users;
        session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        criteriaQuery.select(userRoot).where(userRoot.get(USER_ROLE).in(roles));
        users = session.createQuery(criteriaQuery)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
        return users;
    }

    @Override
    public Collection<User> findByDepartments(int offset,
                                              int limit,
                                              Collection<Long> departmentIds) {
        Session session;
        Collection<User> users;
        session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        Join<User, Department> departmentJoin = userRoot.join(DEPARTMENT);
        criteriaQuery.select(userRoot).where(departmentJoin.get(ID).in(departmentIds));
        users = session.createQuery(criteriaQuery)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
        return users;
    }

    @Override
    public Collection<User> findByDepartmentsAndRoles(int offset,
                                                      int limit,
                                                      Collection<Long> departmentIds,
                                                      Collection<UserRole> roles) {
        Session session;
        Collection<User> users;
        session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        Join<User, Department> departmentJoin = userRoot.join(DEPARTMENT);
        List<Predicate> conditions = new ArrayList();
        conditions.add(departmentJoin.get(ID).in(departmentIds));
        conditions.add(userRoot.get(USER_ROLE).in(roles));
        criteriaQuery.select(userRoot).where(conditions.toArray(new Predicate[]{}));
        users = session.createQuery(criteriaQuery)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
        return users;
    }

    @Override
    public Collection<PrivateItem> getUserPrivateItems(Long userId) {
        Session session;
        Collection<PrivateItem> privateItems;
        session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<PrivateItem> criteriaQuery = builder.createQuery(PrivateItem.class);
        Root<PrivateItem> privateItemRoot = criteriaQuery.from(PrivateItem.class);
        Join<PrivateItem, User> userJoin = privateItemRoot.join("owner");
        criteriaQuery.select(privateItemRoot).where(builder.equal(userJoin.get(ID), userId));
        privateItems = session.createQuery(criteriaQuery).getResultList();
        return privateItems;
    }

    @Override
    public Long getUsersCount() {
        Long userCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        criteriaQuery
                .select(builder.count(userRoot));
        userCount = session.createQuery(criteriaQuery).getSingleResult();

        return userCount;
    }

    @Override
    public Long getUsersFilteredByRolesCount(Collection<UserRole> roles) {
        Long userCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<User> userRoot = criteriaQuery.from(User.class);

        criteriaQuery
                .select(builder.count(userRoot))
                .where(userRoot.get(USER_ROLE).in(roles));

        userCount = session.createQuery(criteriaQuery).getSingleResult();

        return userCount;
    }

    @Override
    public Long getUsersFilteredByDepartmentsCount(Collection<Long> departmentIds) {
        Long userCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        Join<User, Department> departmentJoin = userRoot.join(DEPARTMENT);

        criteriaQuery
                .select(builder.count(userRoot))
                .where(departmentJoin.get(ID).in(departmentIds));

        userCount = session.createQuery(criteriaQuery).getSingleResult();

        return userCount;
    }

    @Override
    public Long getUsersFilteredByDepartmentAndRolesCount(Collection<Long> departmentIds, Collection<UserRole> roles) {
        Long userCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        Join<User, Department> departmentJoin = userRoot.join(DEPARTMENT);

        List<Predicate> conditions = new ArrayList<>();

        conditions.add(departmentJoin.get(ID).in(departmentIds));
        conditions.add(userRoot.get(USER_ROLE).in(roles));

        criteriaQuery
                .select(builder.count(userRoot))
                .where(conditions.toArray(new Predicate[]{}));

        userCount = session.createQuery(criteriaQuery).getSingleResult();

        return userCount;
    }

    @Override
    public Long getUserPrivateItemsCount(Long userId) {
        Long itemsCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<PrivateItem> privateItemRoot = criteriaQuery.from(PrivateItem.class);
        Join<PrivateItem, User> userJoin = privateItemRoot.join("owner");

        criteriaQuery
                .select(builder.count(privateItemRoot))
                .where(builder.equal(userJoin.get(ID), userId));

        itemsCount = session.createQuery(criteriaQuery).getSingleResult();

        return itemsCount;
    }

    @Override
    public User getByLogin(String login) {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
            Root<User> userRoot = query.from(User.class);
            query.select(userRoot).where(userRoot.get("login").in(login));

            return session.createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            logger.info("Unsuccessful try to get User by login. %s" + e.getLocalizedMessage());
            return null;
        }
    }
}
