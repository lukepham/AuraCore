package academy.softserve.aura.core.dao.impl;

import academy.softserve.aura.core.dao.CommonItemDao;
import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.Collection;

@Repository
public class CommonItemDaoImpl extends CrudDaoImpl<CommonItem> implements CommonItemDao {

    @Autowired
    private SessionFactory sessionFactory;

    public CommonItemDaoImpl() {
        super(CommonItem.class);
    }

    @Override
    public Collection<CommonItem> findFewByIsWorking(int offset, int limit, boolean isWorking) {
        Session session;
        Collection<CommonItem> commonItems;
            session = sessionFactory.getCurrentSession();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CommonItem> cq = cb.createQuery(CommonItem.class);
            Root<CommonItem> commonItemRoot = cq.from(CommonItem.class);
            cq.select(commonItemRoot)
                    .where(cb.equal(commonItemRoot.get("working"),isWorking));
            commonItems = session.createQuery(cq)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();
        return commonItems;
    }

    @Override
    public Collection<Department> getCommonItemDepartments(Long itemID) {
        Collection<Department> departments;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Department> criteriaQuery = builder.createQuery(Department.class);
        Root<CommonItem> commonItemRoot = criteriaQuery.from(CommonItem.class);
        Join<CommonItem, Department> departmentJoin = commonItemRoot.join("allDepartmentsWhichOwnedCommonItem");

        criteriaQuery
                .select(departmentJoin)
                .where(builder.equal(commonItemRoot.get("id"),itemID));
        departments = session.createQuery(criteriaQuery).getResultList();
        return departments;
    }

    @Override
    public Long getCommonItemsFilteredByIsWorkingCount(boolean isWorking) {
        Long itemsCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<CommonItem> itemRoot = criteriaQuery.from(CommonItem.class);

        criteriaQuery
                .select(builder.count(itemRoot))
                .where(builder.equal(itemRoot.get("working"),isWorking));

        itemsCount = session.createQuery(criteriaQuery).getSingleResult();

        return itemsCount;
    }

    @Override
    public boolean assignCommonItemToWarehouse(CommonItem commonItem) {
        commonItem.getComponents().forEach(item -> assignCommonItemToWarehouse((CommonItem)item));
        Session session = sessionFactory.getCurrentSession();
        session.createNativeQuery("delete from common_item where item_id = :item_id")
                .setParameter("item_id", commonItem.getId())
                .executeUpdate();
        return true;
    }

}
