package academy.softserve.aura.core.dao;

import java.util.Collection;

/**
 * The basic interface for CRUD operations.
 * It is a root interface in the hierarchy of DAO interfaces.
 *
 * @param <E>  the type of Entity
 *
 * @author Ivan Vakhovskyi
 */
public interface CrudDao<E> {

    /**
     * INSERT operation for single entity.
     *
     * @param element which is entity object to be represented in DB
     * @return created and fetched from DB entity object
     */
    E addElement(E element);

    /**
     * INSERT operation for collection of entities.
     *
     * @param elements collection of entities to be represented in DB
     * @return collection of added entities to DB
     */
    Collection<E> addAll( Collection<E> elements);

    /**
     * UPDATE operation for single entity.
     *
     * @param element entity that needs to be updated
     * @return updated and fetched from DB entity object
     */
    E updateElement(E element);

    /**
     * SELECT operation for single entity.
     *
     * @param elementId which is ID of the entity represented in DB
     * @return entity fetched from DB
     */
    E getElementByID(Long elementId);

    /**
     * SELECT operation for all entities.
     *
     * @return the collection of entities fetched from DB
     */
    Collection<E> getAllElements();

    /**
     * DELETE action for single entity.
     *
     * @param id of the entity represented in DB
     * @return true in case of success, false otherwise
     */
    boolean deleteElement(Long id);

    /**
     * SELECT operation for few entities.
     *
     * @param offSet the point of first entry to return from a collection
     * @param limit  the number of entries to return from a collection
     * @return the collection of entities fetched from DB by parameters
     */
    Collection<E> getFew(int offSet, int limit);

    /**
     * Gets number of records in database.
     *
     * @return {@link Long} count of records in database.
     */
    Long getRecordsCount();

}
