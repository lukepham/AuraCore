package academy.softserve.aura.core.dao.impl;


import academy.softserve.aura.core.dao.ItemLogDao;
import academy.softserve.aura.core.entity.ItemLog;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.OffsetDateTime;
import java.util.Collection;

@Repository
public class ItemLogDaoImpl extends CrudDaoImpl<ItemLog> implements ItemLogDao{

    private static final String ITEM_ID = "itemId";
    private static final String EVENT_DATE = "eventDate";

    @Autowired
    private SessionFactory sessionFactory;

    public ItemLogDaoImpl() {
        super(ItemLog.class);
    }

    @Override
    public Collection<ItemLog> getAllSinceTime(OffsetDateTime sinceTime) {
        Session session = sessionFactory.getCurrentSession();
        Collection<ItemLog> elements;
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<ItemLog> criteria = builder.createQuery(ItemLog.class);
        Root<ItemLog> elementRoot = criteria.from(ItemLog.class);
        criteria
                .select(elementRoot)
                .where(builder.greaterThanOrEqualTo(elementRoot.get(EVENT_DATE), sinceTime));
        elements = session.createQuery(criteria).getResultList();
        return elements;
    }

    @Override
    public Collection<ItemLog> getAllByItemId(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Collection<ItemLog> elements;
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<ItemLog> criteria = builder.createQuery(ItemLog.class);
        Root<ItemLog> elementRoot = criteria.from(ItemLog.class);
        criteria
                .select(elementRoot)
                .where(builder.equal(elementRoot.get(ITEM_ID), id.toString()));
        elements = session.createQuery(criteria).getResultList();
        return elements;
    }

    @Override
    public Collection<ItemLog> getAllByItemIdAndTime(Long id, OffsetDateTime sinceTime) {
        Session session = sessionFactory.getCurrentSession();
        Collection<ItemLog> elements;
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<ItemLog> criteria = builder.createQuery(ItemLog.class);
        Root<ItemLog> elementRoot = criteria.from(ItemLog.class);
        criteria
                .select(elementRoot)
                .where(builder.equal(elementRoot.get(ITEM_ID), id.toString()),
                        builder.greaterThanOrEqualTo(elementRoot.get(EVENT_DATE), sinceTime));
        elements = session.createQuery(criteria).getResultList();
        return  elements;
    }
}
