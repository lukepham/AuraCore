package academy.softserve.aura.core.dao.impl;


import academy.softserve.aura.core.dao.UserCommonItemEventDao;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserCommonItemEvent;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.OffsetDateTime;
import java.util.Collection;

import static academy.softserve.aura.core.utils.Constants.*;

@Repository
public class UserCommonItemEventDaoImpl extends CrudDaoImpl<UserCommonItemEvent> implements UserCommonItemEventDao {

    @Autowired
    private SessionFactory sessionFactory;

    public UserCommonItemEventDaoImpl() {
        super(UserCommonItemEvent.class);
    }

    @Override
    public Collection<UserCommonItemEvent> getAllSinceTime(OffsetDateTime sinceTime, int offset, int limit) {
        Session session = sessionFactory.getCurrentSession();
        Collection<UserCommonItemEvent> elements;
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<UserCommonItemEvent> criteria = builder.createQuery(UserCommonItemEvent.class);
        Root<UserCommonItemEvent> elementRoot = criteria.from(UserCommonItemEvent.class);

        criteria
                .select(elementRoot)
                .where(builder.greaterThanOrEqualTo(elementRoot.get(USAGE_START_DATE), sinceTime));
        elements = session.createQuery(criteria)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return elements;
    }

    @Override
    public Collection<UserCommonItemEvent> getAllByUserId(Long id, int offset, int limit) {
        Session session = sessionFactory.getCurrentSession();
        Collection<UserCommonItemEvent> elements;
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<UserCommonItemEvent> criteria = builder.createQuery(UserCommonItemEvent.class);
        Root<UserCommonItemEvent> elementRoot = criteria.from(UserCommonItemEvent.class);
        Join<User, UserCommonItemEvent> userJoin = elementRoot.join(USER);

        criteria
                .select(elementRoot)
                .where(builder.equal(userJoin.get(ID), id));
        elements = session.createQuery(criteria)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return elements;
    }

    @Override
    public Collection<UserCommonItemEvent> getAllByUserIdAndTime(Long id, OffsetDateTime sinceTime, int offset,
                                                                 int limit) {
        Session session = sessionFactory.getCurrentSession();
        Collection<UserCommonItemEvent> elements;
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<UserCommonItemEvent> criteria = builder.createQuery(UserCommonItemEvent.class);
        Root<UserCommonItemEvent> elementRoot = criteria.from(UserCommonItemEvent.class);
        Join<User, UserCommonItemEvent> userJoin = elementRoot.join(USER);

        criteria
                .select(elementRoot)
                .where(builder.equal(userJoin.get(ID), id),
                        builder.greaterThanOrEqualTo(elementRoot.get(USAGE_START_DATE), sinceTime));
        elements = session.createQuery(criteria)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return  elements;
    }

    @Override
    public Long getAllSinceTimeCount(OffsetDateTime sinceTime) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
        Root<UserCommonItemEvent> elementRoot = criteria.from(UserCommonItemEvent.class);

        criteria
                .select(builder.count(elementRoot))
                .where(builder.greaterThanOrEqualTo(elementRoot.get(USAGE_START_DATE), sinceTime));

        return session.createQuery(criteria).getSingleResult();
    }

    @Override
    public Long getAllByUserIdCount(Long id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
        Root<UserCommonItemEvent> elementRoot = criteria.from(UserCommonItemEvent.class);
        Join<User, UserCommonItemEvent> userJoin = elementRoot.join(USER);

        criteria
                .select(builder.count(elementRoot))
                .where(builder.equal(userJoin.get(ID), id));

        return session.createQuery(criteria).getSingleResult();
    }

    @Override
    public Long getAllByUserIdAndTimeCount(Long id, OffsetDateTime sinceTime) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
        Root<UserCommonItemEvent> elementRoot = criteria.from(UserCommonItemEvent.class);
        Join<User, UserCommonItemEvent> userJoin = elementRoot.join(USER);

        criteria
                .select(builder.count(elementRoot))
                .where(builder.equal(userJoin.get(ID), id),
                        builder.greaterThanOrEqualTo(elementRoot.get(USAGE_START_DATE), sinceTime));

        return  session.createQuery(criteria).getSingleResult();
    }

}
