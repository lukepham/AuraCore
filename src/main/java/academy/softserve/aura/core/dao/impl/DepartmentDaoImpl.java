package academy.softserve.aura.core.dao.impl;

import academy.softserve.aura.core.dao.DepartmentDao;
import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.Collection;

@Repository
public class DepartmentDaoImpl extends CrudDaoImpl<Department> implements DepartmentDao {

    private static final String DEPARTMENT = "department";
    private static final String ID = "id";

    @Autowired
    private SessionFactory sessionFactory;

    public DepartmentDaoImpl() {
        super(Department.class);
    }

    @Override
    public Collection<CommonItem> getDepartmentCommonItems(Long departmentId) {

        Session session;
        Collection<CommonItem> commonItems;
            session = sessionFactory.getCurrentSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<CommonItem> criteriaQuery = builder.createQuery(CommonItem.class);
            Root<CommonItem> commonItemRoot = criteriaQuery.from(CommonItem.class);
            Join<CommonItem, Department> departmentJoin = commonItemRoot.join(DEPARTMENT);
            criteriaQuery.select(commonItemRoot).where(builder.equal(departmentJoin.get(ID), departmentId));
            commonItems = session.createQuery(criteriaQuery).getResultList();
        return commonItems;
    }

    @Override
    public Collection<User> getDepartmentUsers(Long departmentId) {

        Session session;
        Collection<User> users;
            session = sessionFactory.getCurrentSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
            Root<User> userRoot = criteriaQuery.from(User.class);
            Join<User, Department> departmentJoin = userRoot.join(DEPARTMENT);
            criteriaQuery.select(userRoot).where(builder.equal(departmentJoin.get(ID), departmentId));
            users = session.createQuery(criteriaQuery).getResultList();
        return users;
    }

    @Override
    public Long getDepartmentCommonItemsCount(Long departmentId) {
        Long departmentCommonItemsCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);

        Root<CommonItem> commonItemRoot = criteriaQuery.from(CommonItem.class);
        Join<CommonItem, Department> departmentJoin = commonItemRoot.join(DEPARTMENT);

        criteriaQuery
                .select(builder.count(commonItemRoot))
                .where(builder.equal(departmentJoin.get(ID), departmentId));

        departmentCommonItemsCount = session.createQuery(criteriaQuery).getSingleResult();

        return departmentCommonItemsCount;
    }

    @Override
    public Long getDepartmentUsersCount(Long departmentId) {
        Long departmentUsersCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        Join<User, Department> departmentJoin = userRoot.join(DEPARTMENT);

        criteriaQuery
                .select(builder.count(userRoot))
                .where(builder.equal(departmentJoin.get(ID), departmentId));

        departmentUsersCount = session.createQuery(criteriaQuery).getSingleResult();

        return departmentUsersCount;
    }

}
