package academy.softserve.aura.core.exceptions.handlers;

import academy.softserve.aura.core.exceptions.AuraPermissionsException;
import academy.softserve.aura.core.exceptions.AuraUserException;
import academy.softserve.aura.core.exceptions.ItemNotFoundException;
import academy.softserve.aura.core.security.handlers.ResponseMessageUtils;
import academy.softserve.aura.core.swagger.model.ErrorDto;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.Set;

import static academy.softserve.aura.core.utils.Constants.LOGIN_PATH;

/**
 * Bean contains handlers for exceptions, thrown from beans annotated with {@link org.springframework.stereotype.Controller}
 * and from another places such as {@link org.springframework.security.web.FilterChainProxy}
 * in this case axception should be mapped manually
 */
@ControllerAdvice
public class GlobalAuraExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalAuraExceptionHandler.class);

    protected final HttpServletRequest request;

    @Autowired
    public GlobalAuraExceptionHandler(HttpServletRequest request) {
        this.request = request;
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    protected ResponseEntity<ErrorDto> handleConflict(ConstraintViolationException ex) {
        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
        ErrorDto errorDto = new ErrorDto();

        for (ConstraintViolation violation : constraintViolations) {
            String argument = ((PathImpl) violation.getPropertyPath()).getLeafNode().getName();
            logger.info(violation.getRootBeanClass().getName() + ": " + argument + " " + violation.getMessage());
        }

        errorDto.setCode(HttpStatus.BAD_REQUEST.value());
        errorDto.setMessage("invalid input");

        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {AuraUserException.class})
    protected ResponseEntity<ErrorDto> handleUserException(AuraUserException ex) {
        return makeErrorDto(ex, HttpStatus.BAD_REQUEST, this.request);
    }


    @ExceptionHandler(value = {AuraPermissionsException.class})
    protected ResponseEntity<ErrorDto> handleUserPermissionException(AuraPermissionsException ex) {
        return makeErrorDto(ex, HttpStatus.FORBIDDEN, this.request);
    }

    @ExceptionHandler(value = {AuthenticationException.class})
    public ResponseEntity<ErrorDto> handleAuthenticationException(AuthenticationException ex) throws IOException {
       return makeErrorDto(ex, HttpStatus.UNAUTHORIZED, this.request);
    }


    public void handleAuthenticationException(AuthenticationException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
        makeAndSendErrorDto(ex, HttpStatus.UNAUTHORIZED, request, response);
    }


    public void handleAccessDeniedException(AccessDeniedException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
        makeAndSendErrorDto(ex, HttpStatus.FORBIDDEN, request, response);
    }

    /**
     * form {@link ResponseEntity<ErrorDto> } from
     *
     * @param exception
     * @param request   request, which caused problem
     */
    private ResponseEntity<ErrorDto> makeErrorDto(RuntimeException exception, HttpStatus status, HttpServletRequest request) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setCode(status.value());
        errorDto.setMessage(exception.getLocalizedMessage());
        ResponseEntity<ErrorDto> errorDtoResponseEntity = new ResponseEntity<>(errorDto, status);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null)
            logger.warn("error of type {} occured, sending {}",
                    exception.getClass().getName(),
                    errorDtoResponseEntity
            );
        else
            logger.warn("User with login {} has error of type {}, sending {}",
                    authentication.getName(),
                    exception.getClass().getName(),
                    errorDtoResponseEntity
            );

        return errorDtoResponseEntity;
    }


    /**
     * used to map
     *
     * @param exception to {@link ErrorDto} and write it to
     * @param response
     */
    private void makeAndSendErrorDto(RuntimeException exception, HttpStatus status, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ResponseEntity<ErrorDto> errorDtoResponseEntity = makeErrorDto(exception, status, request);
        ResponseMessageUtils.sendResponseMessage(response, status, errorDtoResponseEntity.getBody().toString().replace("class ErrorDto ", ""));
    }

    @ExceptionHandler(value = {ItemNotFoundException.class})
    protected ResponseEntity<ErrorDto> handleItemNotFoundException(ItemNotFoundException ex) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setCode(HttpStatus.BAD_REQUEST.value());
        errorDto.setMessage(ex.getMessage());
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
                                                             HttpStatus status, WebRequest request) {
        logger.info(ex.getLocalizedMessage());
        return super.handleExceptionInternal(ex, body, headers, status, request);
    }

}
