package academy.softserve.aura.core.exceptions;

import academy.softserve.aura.core.entity.User;

/**
 *Thrown when an {@link User} object processing happens *
 *
 * @author  Eugene Savchenko
 */

public class AuraUserException extends AuraException {

    /** Constructs a new {@code AuraUserException} with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public AuraUserException (){
        super();
    }

    /** Constructs a new {@code AuraUserException} with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param   message   the detail message. The detail message is saved for
     *          later retrieval by the {@link #getMessage()} method.
     */
    public AuraUserException(String message) {
        super(message);
    }
}