package academy.softserve.aura.core.exceptions;

/**
 * Thrown when user tries to add common item usage event with common item id which is not present in database.
 *
 * @author Yana Kostiuk
 */
public class ItemNotFoundException extends AuraException {

    public ItemNotFoundException() {
        super();
    }

    public ItemNotFoundException(String message) {
        super(message);
    }
}
