package academy.softserve.aura.core.exceptions;


/**
 * Thrown when user who calls method does not have needed {@link academy.softserve.aura.core.entity.UserRole}
 * to perform operation
 *
 * @author Olexandr Brytskyi
 */
public class AuraPermissionsException extends AuraException {

    public AuraPermissionsException() {
        super();
    }

    public AuraPermissionsException(String message) {
        super(message);
    }
}
