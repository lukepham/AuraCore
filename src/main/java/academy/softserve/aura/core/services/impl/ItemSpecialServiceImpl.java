package academy.softserve.aura.core.services.impl;


import academy.softserve.aura.core.dao.ItemDao;
import academy.softserve.aura.core.entity.*;
import academy.softserve.aura.core.services.DepartmentService;
import academy.softserve.aura.core.services.ItemLogService;
import academy.softserve.aura.core.services.UserService;
import academy.softserve.aura.core.services.messaging.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ItemSpecialServiceImpl<E> extends CrudServiceImpl<E> {

    @Autowired
    private ItemLogService itemLogService;

    @Autowired
    private MessageSender messageSender;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private UserService userService;

    @Autowired
    private DepartmentService departmentService;


    public ItemSpecialServiceImpl(Class<E> elementClass) {
    }

    @Override
    @Transactional
    public E create(E entity) {
        Item item = (Item) entity;
        Long itemId = item.getId();

        setOwnerToHierarchy(item);

        if (itemId != null && itemDao.getElementByID(itemId) != null) {
            return update(entity);
        }

        List<ItemLog> logList = new ArrayList<>();
        itemLogService.generateUpgradedLogsForHierarchy(item, logList);
        List<String> upgradedItems = new ArrayList<>();
        logList.forEach(itemLog -> upgradedItems.add(itemLog.getItemId()));

        itemDao.addElement(item);

        itemLogService.generateCreatedLogsForHierarchy(item, logList, upgradedItems);

        itemLogService.createAll(logList);

        for (ItemLog itemLog : logList) {
            itemLog.setItem(itemDao.getElementByID(Long.parseLong(itemLog.getItemId())));
            messageSender.sendMessage(itemLog);
        }

        return entity;
    }

    @Override
    @Transactional
    public Collection<E> createAll(Collection<E> entities) {
        Collection<Item> items = (Collection<Item>) entities;
        items.forEach(this::setOwnerToHierarchy);

        List<ItemLog> logList = new ArrayList<>();
        items.forEach(item -> itemLogService.generateUpgradedLogsForHierarchy(item, logList));
        List<String> upgradedItems = new ArrayList<>();
        logList.forEach(itemLog -> upgradedItems.add(itemLog.getItemId()));

        itemDao.addAll(items);

        items.forEach(item -> itemLogService.generateCreatedLogsForHierarchy(item, logList, upgradedItems));
        itemLogService.createAll(logList);

        for (ItemLog itemLog : logList) {
            itemLog.setItem(itemDao.getElementByID(Long.parseLong(itemLog.getItemId())));
            messageSender.sendMessage(itemLog);
        }

        return entities;
    }

    @Override
    @Transactional
    public E findById(Long id) {
        return (E) itemDao.getElementByID(id);
    }

    @Override@Transactional

    public Collection<E> findAll() {
        return (Collection<E>) itemDao.getAllElements();
    }

    @Override
    @Transactional
    public Collection<E> findFew(int offset, int limit) {
        return (Collection<E>) itemDao.getFew(offset, limit);
    }

    @Override
    @Transactional
    public E update(E entity) {
        Item item = (Item) entity;
        Long itemId = item.getId();

        setOwnerToHierarchy(item);

        if (itemId == null || itemDao.getElementByID(itemId) == null) {
            return create(entity);
        }

        for (Item component : item.getComponents()) {
            if (component.getId() == null) {
                create((E) component);
            }
        }

        item.setParentItem(itemDao.getElementByID(itemId).getParentItem());

        boolean oldWorkingStatus = itemDao.getElementByID(itemId).isWorking();

        Item updated = itemDao.updateElement(item);

        boolean updatedWorkingStatus = updated.isWorking();
        if (oldWorkingStatus && !updatedWorkingStatus) {
            logAndSendMessage(item,  OffsetDateTime.now(), ItemEventType.ITEM_BROKEN, "Item is broken");
        } else if (!oldWorkingStatus && updatedWorkingStatus) {
            logAndSendMessage(item, OffsetDateTime.now(), ItemEventType.ITEM_REPAIRED, "Item is repaired");
        } else {
            logAndSendMessage(item, OffsetDateTime.now(), ItemEventType.ITEM_UPDATED, "Item is updated");
        }

        if (updated.getParentItem() != null) {
            List<ItemLog> logList = new ArrayList<>();
            itemLogService.generateAndSaveUpdateLogsForParent(updated, logList);

            for (ItemLog itemLog : logList) {
                itemLog.setItem(itemDao.getElementByID(Long.parseLong(itemLog.getItemId())));
                messageSender.sendMessage(itemLog);
            }
        }

        return entity;
    }

    private void setOwnerToHierarchy(Item entity) {
        if(entity.getClass() == PrivateItem.class) {
            PrivateItem privateItem = (PrivateItem) entity;
            privateItem.setOwner(userService.findById(privateItem.getOwner().getId()));
            if (!privateItem.getComponents().isEmpty()) {
                privateItem.getComponents().forEach(this::setOwnerToHierarchy);
            }
        } else if (entity.getClass() == CommonItem.class) {
            CommonItem commonItem = (CommonItem) entity;
            commonItem.setDepartment(departmentService.findById(commonItem.getDepartment().getId()));
            if (!commonItem.getComponents().isEmpty()) {
                commonItem.getComponents().forEach(this::setOwnerToHierarchy);
            }
        }
    }

    @Override
    @Transactional
    public boolean delete(Long id) {
        Item item = itemDao.getElementByID(id);
        Item parentItem = item.getParentItem();

        setOwnerToHierarchy(item);

        if (parentItem != null){
            parentItem.getComponents().remove(item);
            item.setParentItem(null);
            itemDao.updateElement(parentItem);

            logAndSendMessage(parentItem, OffsetDateTime.now(), ItemEventType.ITEM_DELETED, "Component deleted. ID of component: " + id);
        }

        boolean result = itemDao.deleteElement(id);

        logAndSendMessage(item, OffsetDateTime.now(), ItemEventType.ITEM_DELETED, "Item is deleted");

        if (!item.getComponents().isEmpty()) {
            for (Item component : item.getComponents()) {
                logAndSendMessage(component, OffsetDateTime.now(), ItemEventType.ITEM_CREATED, "Item is deleted");
            }
        }

        return result;
    }

    @Transactional
    public void logAndSendMessage(Item item, OffsetDateTime dateTime, ItemEventType eventType, String description) {
        ItemLog simpleItemLog = itemLogService.createAndSaveSimpleItemLog(item.getId().toString(), dateTime, eventType, description);
        simpleItemLog.setItem(item);

        messageSender.sendMessage(simpleItemLog);
    }
}
