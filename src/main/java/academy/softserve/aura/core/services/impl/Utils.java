package academy.softserve.aura.core.services.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);
    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String ROLE_MANAGER = "ROLE_MANAGER";
    private static final String ROLE_USER = "ROLE_USER";

    private Utils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * give a possibility to determine Authorities of authorized person who called the method
     *
     * @return {@link Collection<SimpleGrantedAuthority>} of user who called method or
     * null if there is no any authentification
     */
    public static Collection<SimpleGrantedAuthority> getUserWhoCalledMethodAuthorities() {
        try {
            return
                    (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext()
                            .getAuthentication().getAuthorities();
        } catch (NullPointerException e) {
            LOGGER.info(String.format(
                    "User with login:\'%s\' authentification has not any roles, returning null",
                    ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()));
            return null;
        }
    }

    /**
     * Checks whether user has admin rights.
     *
     * @return {@link boolean} whether user has admin rights
     */
    public static boolean isAdmin() {
        Collection<SimpleGrantedAuthority> authorities = getUserWhoCalledMethodAuthorities();
        return authorities != null && authorities.stream().anyMatch(a -> a.getAuthority().equals(ROLE_ADMIN));
    }

    /**
     * Checks whether user has manager rights.
     *
     * @return {@link boolean} whether user has manager rights
     */
    public static boolean isManager() {
        Collection<SimpleGrantedAuthority> authorities = getUserWhoCalledMethodAuthorities();
        return authorities != null && authorities.stream().anyMatch(a -> a.getAuthority().equals(ROLE_MANAGER));
    }

    /**
     * Checks whether user has user rights.
     *
     * @return {@link boolean} whether user has user rights
     */
    public static boolean isUser() {
        Collection<SimpleGrantedAuthority> authorities = getUserWhoCalledMethodAuthorities();
        return authorities != null && authorities.stream().anyMatch(a -> a.getAuthority().equals(ROLE_USER));
    }

    /**
     * Returns login of logged in user.
     *
     * @return {@link String} login of logged in user.
     */
    public static String getUserLogin() {
        org.springframework.security.core.userdetails.User principal =
                (org.springframework.security.core.userdetails.User)
                        SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return principal.getUsername();
    }

}
