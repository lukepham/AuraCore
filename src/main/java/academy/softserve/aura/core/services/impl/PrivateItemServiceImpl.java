package academy.softserve.aura.core.services.impl;

import academy.softserve.aura.core.dao.PrivateItemDao;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.services.PrivateItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Collection;

@Service
public class PrivateItemServiceImpl extends ItemSpecialServiceImpl<PrivateItem> implements PrivateItemService {

    @Autowired
    private PrivateItemDao privateItemDao;

    public PrivateItemServiceImpl() {
        super(PrivateItem.class);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<PrivateItem> findFewByIsWorking(int offset, int limit, boolean isWorking) {
        return privateItemDao.findFewByIsWorking(offset, limit, isWorking);
    }

    @Override
    @Transactional
    public Collection<PrivateItem> findFew(int offset, int limit) {
        return privateItemDao.getFew(offset, limit);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<User> getItemUsers(Long id) {
        return privateItemDao.getPrivateItemUsers(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getPrivateItemsCount() {
        return privateItemDao.getRecordsCount();
    }

    @Override
    @Transactional(readOnly = true)
    public Long getPrivateItemsFilteredByIsWorkingCount(boolean isWorking) {
        return privateItemDao.getPrivateItemsFilteredByIsWorkingCount(isWorking);
    }

    @Override
    @Transactional
    public boolean assignPrivateItemToWarehouse(PrivateItem privateItem) {
        boolean isAddedToDB = privateItemDao.assignPrivateItemToWarehouse(privateItem);

        if(isAddedToDB) {
            privateItem.setOwner(null);
            logAndSendMessage(privateItem, OffsetDateTime.now(), ItemEventType.OWNER_CHANGED, "Item is moved to Warehouse");

            privateItem.getComponents().forEach(item -> logAndSendMessage(item, OffsetDateTime.now(), ItemEventType.OWNER_CHANGED, "Item is moved to Warehouse"));
        }

        return isAddedToDB;
    }
}