package academy.softserve.aura.core.services;


import java.util.Collection;


/**
 * The basic interface for CRUD operations.
 * It is a root interface in the hierarchy of Service interfaces.
 *
 * @param <E>  the type of Entity
 */
public interface CrudService<E> {

    /**
     * INSERT action for single entity.
     *
     * @param entity the entity to be represented in DB
     * @return created and fetched from DB entity object
     */
    E create(E entity);

    /**
     * INSERT action for collection of entities.
     *
     * @param entities collection of entities to be represented in DB
     * @return true in case of success, false in other case
     */
    Collection<E> createAll(Collection<E> entities);

    /**
     * SELECT operation for single entity.
     *
     * @param id of the entity represented in DB
     * @return entity fetched from DB
     */
    E findById(Long id);

    /**
     * SELECT operation for all entities.
     *
     * @return the collection of all entities represented in DB
     */
    Collection<E> findAll();

    /**
     * SELECT operation for few entities.
     *
     * @param offset the first entry to return from a collection
     * @param limit  the quantity of entries to return from a collection
     * @return the collection of entities fetched from DB by parameters
     */
    Collection<E> findFew(int offset, int limit);

    /**
     * UPDATE action for single entity.
     *
     * @param entity updated entity
     * @return updated and fetched from DB entity object
     */
    E update(E entity);

    /**
     * DELETE action for single entity.
     *
     * @param id of the entity represented in DB
     * @return true in case of success, false in other case
     */
    boolean delete(Long id);

    /**
     * Gets number of records in database.
     *
     * @return {@link Long} count of records in database.
     */
    Long getRecordsCount();

}