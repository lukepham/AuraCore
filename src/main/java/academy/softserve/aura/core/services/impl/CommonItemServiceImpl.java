package academy.softserve.aura.core.services.impl;

import academy.softserve.aura.core.dao.CommonItemDao;
import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.services.CommonItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Collection;

@Service
public class CommonItemServiceImpl extends ItemSpecialServiceImpl<CommonItem> implements CommonItemService {

    @Autowired
    private CommonItemDao commonItemDao;

    public CommonItemServiceImpl() {
        super(CommonItem.class);
    }

    @Override
    @Transactional
    public Collection<CommonItem> findFewByIsWorking(int offset, int limit, boolean isWorking) {
        return commonItemDao.findFewByIsWorking(offset, limit, isWorking);
    }

    @Override
    @Transactional
    public Collection<CommonItem> findFew(int offset, int limit) {
        return commonItemDao.getFew(offset, limit);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Department> getItemDepartments(Long id) {
        return commonItemDao.getCommonItemDepartments(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getCommonItemsCount() {
        return commonItemDao.getRecordsCount();
    }

    @Override
    @Transactional(readOnly = true)
    public Long getCommonItemsFilteredByIsWorkingCount(boolean isWorking) {
        return commonItemDao.getCommonItemsFilteredByIsWorkingCount(isWorking);
    }

    @Override
    @Transactional
    public boolean assignCommonItemToWarehouse(CommonItem commonItem) {
        boolean isAddedToDB = commonItemDao.assignCommonItemToWarehouse(commonItem);

        if(isAddedToDB) {
            commonItem.setDepartment(null);
            logAndSendMessage(commonItem, OffsetDateTime.now(), ItemEventType.OWNER_CHANGED, "Item is moved to Warehouse");

            commonItem.getComponents().forEach(item -> logAndSendMessage(item, OffsetDateTime.now(), ItemEventType.OWNER_CHANGED, "Item is moved to Warehouse"));
        }

        return isAddedToDB;
    }
}