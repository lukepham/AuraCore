package academy.softserve.aura.core.services.impl;

import academy.softserve.aura.core.dao.ItemLogDao;
import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.entity.ItemLog;
import academy.softserve.aura.core.services.ItemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

@Service
public class ItemLogServiceImpl extends CrudServiceImpl<ItemLog> implements ItemLogService {

    @Autowired
    private ItemLogDao itemLogDao;

    @Override
    public ItemLog createAndSaveSimpleItemLog(String itemId, OffsetDateTime dateTime, ItemEventType eventType, String message) {
        return itemLogDao.addElement(new ItemLog(itemId, OffsetDateTime.now(), eventType, message));
    }

    @Override
    public Collection<ItemLog> getAllSinceTime(OffsetDateTime sinceTime) {
        return itemLogDao.getAllSinceTime(sinceTime);
    }

    @Override
    public Collection<ItemLog> getAllByItemId(Long id) {
        return itemLogDao.getAllByItemId(id);
    }

    @Override
    public Collection<ItemLog> getAllByItemIdAndTime(Long id, OffsetDateTime sinceTime) {
        return itemLogDao.getAllByItemIdAndTime(id, sinceTime);
    }

    @Override
    public void generateCreatedLogsForHierarchy(Item item, List<ItemLog> logList, List<String> upgradedItems) {
        item.getComponents().forEach(child -> generateCreatedLogsForHierarchy(child, logList, upgradedItems));
        if (!upgradedItems.contains(item.getId().toString())) {
            logList.add(new ItemLog(item.getId().toString(), OffsetDateTime.now(), ItemEventType.ITEM_CREATED, "Item is created"));
        }
    }

    @Override
    public void generateUpgradedLogsForHierarchy(Item item, List<ItemLog> logList) {
        item.getComponents().forEach(child -> generateUpgradedLogsForHierarchy(child, logList));
        if (item.getId() != null) {
            logList.add(new ItemLog(item.getId().toString(), OffsetDateTime.now(), ItemEventType.ITEM_UPDATED, "Item updated. It became component"));
        }
    }

    @Override
    public void generateAndSaveUpdateLogsForParent(Item item, List<ItemLog> logList) {
        Item parent = item.getParentItem();
        ItemLog simpleItemLog = createAndSaveSimpleItemLog
                (parent.getId().toString(), OffsetDateTime.now(), ItemEventType.ITEM_UPDATED, "Some of the components changed");
        logList.add(simpleItemLog);
        if (parent.getParentItem() != null) {
            generateAndSaveUpdateLogsForParent(parent, logList);
        }
    }
}
