package academy.softserve.aura.core.services;

import academy.softserve.aura.core.entity.ItemAmortization;
import academy.softserve.aura.core.entity.ItemAmortizationStatistic;
import academy.softserve.aura.core.swagger.model.ItemAmortizationDto;
import academy.softserve.aura.core.swagger.model.ItemAmortizationStatisticDto;

import java.util.List;

/**
 * The interface for financial calculation and business logic.
 * It make reports, analysis and statistic
 */
public interface FinancialService {

    /**
     * Make calculations which show amortization value between all items
     *
     * @param offset amount of results which should be skipped to start pagination of result
     * @param limit amount of results which should be included to result
     * @return list of {@link ItemAmortization}
     */
    List<ItemAmortization> getAmortizationReport(Integer offset, Integer limit);

    /**
     * Make calculations which show amortization value between one item
     *
     * @param id of item
     * @return {@link ItemAmortizationDto}
     */
    ItemAmortization getAmortizationById (Long id);

    /**
     * Make calculations and generate detailed amortization statistic for one item.
     *
     * @param itemId id of item
     * @param offset amount of results which should be skipped to start pagination of result
     * @param limit amount of results which should be included to result
     * @return list of {@link ItemAmortizationStatisticDto}
     */
    List<ItemAmortizationStatistic> getAmortizationStatisticByItemId(Long itemId, Integer offset, Integer limit);

}