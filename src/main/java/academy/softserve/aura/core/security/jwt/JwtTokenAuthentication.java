package academy.softserve.aura.core.security.jwt;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * POJO implementation of {@link Authentication} which will be retrieved from JWT token and added to
 * {@link org.springframework.security.core.context.SecurityContext} to allow/deny client perform
 * different kinds of operations
 */
public class JwtTokenAuthentication implements Authentication {

    /**
     * JWT token
     */
    private String token;

    /**
     * user authorities (holds user roles)
     */
    private Collection<? extends GrantedAuthority> authorities;

    /**
     * indicates authentication state
     */
    private boolean isAuthenticated;

    /**
     * holds user account details
     */
    private UserDetails principal;

    public JwtTokenAuthentication() {
    }

    public JwtTokenAuthentication(String token, boolean isAuthenticated,
                                  UserDetails principal) {
        this.token = token;
        this.authorities = principal.getAuthorities();
        this.isAuthenticated = isAuthenticated;
        this.principal = principal;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return principal;
    }

    @Override
    public String getName() {
        if (principal != null)
            return ((UserDetails) principal).getUsername();
        else
            return null;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        isAuthenticated = b;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "JwtTokenAuthentication{" +
                "token='" + token + '\'' +
                ", authorities=" + authorities +
                ", isAuthenticated=" + isAuthenticated +
                ", principal=" + principal +
                '}';
    }
}
