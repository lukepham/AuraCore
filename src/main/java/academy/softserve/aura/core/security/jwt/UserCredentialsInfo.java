package academy.softserve.aura.core.security.jwt;


import academy.softserve.aura.core.entity.UserRole;

/**
 * POJO containing info, which should be included to JWT token using {@link JwtUtil#generateToken(String, String, UserCredentialsInfo)}
 */
public class UserCredentialsInfo {

    /**
     * user name will be included to Principal (login)
     */
    private String userName;

    /**
     * user Role will be included to {@link org.springframework.security.core.GrantedAuthority} list
     */
    private UserRole role;

    public UserCredentialsInfo() {
    }

    public UserCredentialsInfo(String userName, UserRole role) {
        this.userName = userName;
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserCredentialsInfo{" +
                "userName='" + userName + '\'' +
                ", role=" + role +
                '}';
    }
}
