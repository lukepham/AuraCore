package academy.softserve.aura.core.security.handlers;


import academy.softserve.aura.core.exceptions.handlers.GlobalAuraExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Handler for {@link AccessDeniedException} thrown from security context
 */
@Component
public class AuraAccessDeniedHandler implements AccessDeniedHandler {

    private final GlobalAuraExceptionHandler exceptionHandler;

    @Autowired
    public AuraAccessDeniedHandler(GlobalAuraExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        exceptionHandler.handleAccessDeniedException(accessDeniedException, request, response);
    }
}
