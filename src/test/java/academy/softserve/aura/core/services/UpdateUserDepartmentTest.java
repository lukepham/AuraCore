package academy.softserve.aura.core.services;

import academy.softserve.aura.core.dao.impl.UserDaoImpl;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.exceptions.AuraPermissionsException;
import academy.softserve.aura.core.services.impl.UserServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class UpdateUserDepartmentTest {

    @Mock
    private UserDaoImpl userDao;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;

    private User loggedInUser;
    private User forUpdate;
    private User fromDatabase;

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Utils.class);

        loggedInUser = EntityFactory.createUserWithCredentialsAndNewDepartment();
        fromDatabase = EntityFactory.createUserWithCredentialsAndNewDepartment();
        forUpdate = EntityFactory.createUserWithCredentialsAndNewDepartment();

        forUpdate.setLogin(null);
        forUpdate.setPassword(null);

        forUpdate.setUserRole(UserRole.USER);
        fromDatabase.setUserRole(UserRole.USER);

        Mockito.when(userDao.getElementByID(forUpdate.getId())).thenReturn(fromDatabase);
    }

    @Test
    public void updateWithSameDepartmentLoggedInAsUser() {
        PowerMockito.when(Utils.isUser()).thenReturn(true);

        forUpdate.getDepartment().setId(fromDatabase.getDepartment().getId());
        userService.update(forUpdate);

        Mockito.verify(userDao, Mockito.times(1)).updateElement(forUpdate);
    }

    @Test(expected = AuraPermissionsException.class)
    public void updateWithDifferentDepartmentLoggedInAsUser() {
        PowerMockito.when(Utils.isUser()).thenReturn(true);

        forUpdate.getDepartment().setId(fromDatabase.getDepartment().getId() + 1);
        userService.update(forUpdate);

        Mockito.verify(userDao, Mockito.times(1)).updateElement(forUpdate);
    }

    @Test
    public void updateWithSameDepartmentLoggedInAsManager() {
        PowerMockito.when(Utils.isUser()).thenReturn(false);
        PowerMockito.when(Utils.isManager()).thenReturn(true);
        PowerMockito.when(Utils.getUserLogin()).thenReturn(loggedInUser.getLogin());

        Mockito.when(userDao.getByLogin(loggedInUser.getLogin())).thenReturn(loggedInUser);

        loggedInUser.getDepartment().setId(fromDatabase.getDepartment().getId());

        forUpdate.getDepartment().setId(fromDatabase.getDepartment().getId());
        userService.update(forUpdate);

        Mockito.verify(userDao, Mockito.times(1)).updateElement(forUpdate);
    }

    @Test(expected = AuraPermissionsException.class)
    public void updateWithDepartmentDifferentFromDatabaseLoggedInAsManager() {
        loggedInUser.getDepartment().setId(fromDatabase.getDepartment().getId() + 1);

        PowerMockito.when(Utils.isUser()).thenReturn(false);
        PowerMockito.when(Utils.isManager()).thenReturn(true);
        PowerMockito.when(Utils.getUserLogin()).thenReturn(loggedInUser.getLogin());

        Mockito.when(userDao.getByLogin(loggedInUser.getLogin())).thenReturn(loggedInUser);

        forUpdate.getDepartment().setId(fromDatabase.getDepartment().getId() + 1);
        userService.update(forUpdate);

        Mockito.verify(userDao, Mockito.times(1)).updateElement(forUpdate);
    }

    @Test(expected = AuraPermissionsException.class)
    public void updateWithDepartmentDifferentFromDtoLoggedInAsManager() {
        loggedInUser.getDepartment().setId(fromDatabase.getDepartment().getId());

        PowerMockito.when(Utils.isUser()).thenReturn(false);
        PowerMockito.when(Utils.isManager()).thenReturn(true);
        PowerMockito.when(Utils.getUserLogin()).thenReturn(loggedInUser.getLogin());

        Mockito.when(userDao.getByLogin(loggedInUser.getLogin())).thenReturn(loggedInUser);

        forUpdate.getDepartment().setId(fromDatabase.getDepartment().getId() + 1);
        userService.update(forUpdate);

        Mockito.verify(userDao, Mockito.times(1)).updateElement(forUpdate);
    }

    @Test(expected = AuraPermissionsException.class)
    public void updateWithoutDepartmentLoggedInAsManager() {
        loggedInUser.setDepartment(null);

        PowerMockito.when(Utils.isUser()).thenReturn(false);
        PowerMockito.when(Utils.isManager()).thenReturn(true);
        PowerMockito.when(Utils.getUserLogin()).thenReturn(loggedInUser.getLogin());
        Mockito.when(userDao.getByLogin(loggedInUser.getLogin())).thenReturn(loggedInUser);

        forUpdate.getDepartment().setId(fromDatabase.getDepartment().getId() + 1);
        userService.update(forUpdate);

        Mockito.verify(userDao, Mockito.times(1)).updateElement(forUpdate);
    }

    @Test(expected = AuraPermissionsException.class)
    public void updateAdminLoggedInAsManager() {
        PowerMockito.when(Utils.isUser()).thenReturn(false);
        PowerMockito.when(Utils.isManager()).thenReturn(true);
        PowerMockito.when(Utils.getUserLogin()).thenReturn(loggedInUser.getLogin());
        Mockito.when(userDao.getByLogin(loggedInUser.getLogin())).thenReturn(loggedInUser);

        forUpdate.setUserRole(UserRole.ADMIN);
        fromDatabase.setUserRole(UserRole.ADMIN);
        forUpdate.getDepartment().setId(fromDatabase.getDepartment().getId() + 1);
        userService.update(forUpdate);

        Mockito.verify(userDao, Mockito.times(1)).updateElement(forUpdate);
    }

    @Test
    public void updateAdminLoggedInAsAdmin() {
        PowerMockito.when(Utils.isUser()).thenReturn(false);
        PowerMockito.when(Utils.isManager()).thenReturn(false);
        PowerMockito.when(Utils.getUserLogin()).thenReturn(loggedInUser.getLogin());
        Mockito.when(userDao.getByLogin(loggedInUser.getLogin())).thenReturn(loggedInUser);

        forUpdate.setUserRole(UserRole.ADMIN);
        fromDatabase.setUserRole(UserRole.ADMIN);
        forUpdate.getDepartment().setId(fromDatabase.getDepartment().getId() + 1);
        userService.update(forUpdate);

        Mockito.verify(userDao, Mockito.times(1)).updateElement(forUpdate);
    }

}
