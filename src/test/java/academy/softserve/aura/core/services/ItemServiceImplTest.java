package academy.softserve.aura.core.services;

import academy.softserve.aura.core.dao.ItemDao;
import academy.softserve.aura.core.entity.*;
import academy.softserve.aura.core.services.impl.ItemServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.services.messaging.MessageSender;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class ItemServiceImplTest {

    @Mock
    private ItemDao itemDao;

    @Mock
    private ItemLogService itemLogService;

    @Mock
    private MessageSender messageSender;

    @Mock
    private PrivateItemService privateItemService;

    @Mock
    private CommonItemService commonItemService;

    @InjectMocks
    private ItemServiceImpl itemService;

    private Item complex, notComplex1, notComplex2;
    private List<Item> itemList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        notComplex1 = EntityFactory.createNotComplexItem();
        notComplex2 = EntityFactory.createNotComplexItem();
        complex = EntityFactory.createComplexItem();
        itemList.add(notComplex1);
        itemList.add(notComplex2);

        Mockito.when(itemLogService.createAndSaveSimpleItemLog(Mockito.anyString(), Mockito.any(),  Mockito.any(), Mockito.anyString()))
                .thenReturn(new ItemLog(complex.getId().toString(), EntityFactory.ITEM_USAGE_START_DATE, ItemEventType.OWNER_CHANGED, "test"));
    }

    @Test
    public void createNotExistingTest() throws Exception {
        notComplex1.setId(null);
        itemService.create(notComplex1);
        Mockito.verify(itemDao, Mockito.times(1)).addElement(notComplex1);
        Mockito.verify(itemLogService, Mockito.times(1)).generateCreatedLogsForHierarchy(Mockito.any(), Mockito.any(),Mockito.any());
        Mockito.verify(itemLogService, Mockito.times(1)).createAll(Mockito.any());
    }

    @Test
    public void createExistingTest() throws Exception {
        Mockito.when(itemDao.getElementByID(notComplex1.getId())).thenReturn(notComplex1);
        Mockito.when(itemDao.updateElement(notComplex1)).thenReturn(notComplex1);

        itemService.create(notComplex1);
        Mockito.verify(itemDao, Mockito.times(1)).updateElement(notComplex1);
        Mockito.verify(itemLogService, Mockito.times(1)).createAndSaveSimpleItemLog(Mockito.any(), Mockito.any(),Mockito.any(),Mockito.any());
    }

    @Test
    public void createAllNotExistingTest() throws Exception {
        itemList.forEach(item -> item.setId(null));
        itemService.createAll(itemList);

        Mockito.verify(itemDao, Mockito.times(1)).addAll(itemList);
        Mockito.verify(itemLogService, Mockito.times(itemList.size())).generateCreatedLogsForHierarchy(Mockito.any(), Mockito.any(),Mockito.any());
        Mockito.verify(itemLogService, Mockito.times(1)).createAll(Mockito.any());
    }

    @Test
    public void createAllExistingTest() throws Exception {
        itemService.createAll(itemList);

        Mockito.verify(itemDao, Mockito.times(1)).addAll(itemList);
        Mockito.verify(itemLogService, Mockito.times(1)).createAll(Mockito.any());
    }

    @Test
    public void findByIdTest() throws Exception {
        itemService.findById(Mockito.anyLong());
        Mockito.verify(itemDao, Mockito.times(1)).getElementByID(Mockito.anyLong());
    }

    @Test
    public void findAllTest() throws Exception {
        itemService.findAll();
        Mockito.verify(itemDao, Mockito.times(1)).getAllElements();
    }

    @Test
    public void findFewTest() throws Exception {
        itemService.findFew(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(itemDao, Mockito.times(1)).getFew(Mockito.anyInt(),Mockito.anyInt());
    }

    @Test
    public void deleteTest() throws Exception {
        Item component = complex.getComponents().get(0);
        notComplex1.setParentItem(component);
        component.setComponents(Arrays.asList(notComplex1));
        Mockito.when(itemDao.getElementByID(component.getId())).thenReturn(component);

        itemService.delete(component.getId());

        Mockito.verify(itemDao, Mockito.times(1)).deleteElement(component.getId());
        Mockito.verify(messageSender, Mockito.times(3)).sendMessage(Mockito.any());
    }

    @Test
    public void updateNotExistingTest() throws Exception {
        complex.setId(null);
        itemService.update(complex);
        Mockito.verify(itemDao, Mockito.times(1)).addElement(Mockito.any());

        complex.getComponents().forEach(item -> item.setId(null));
        itemService.update(complex);
        Mockito.verify(itemDao, Mockito.times(complex.getComponents().size())).addElement(Mockito.any());
    }

    @Test
    public void updateNotifiesParentsTest() throws Exception {
        Item component = complex.getComponents().get(0);
        Mockito.when(itemDao.getElementByID(component.getId())).thenReturn(component);
        Mockito.when(itemDao.updateElement(component)).thenReturn(component);

        itemService.update(component);
        Mockito.verify(itemLogService, Mockito.times(1)).createAndSaveSimpleItemLog(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
        Mockito.verify(itemLogService, Mockito.times(1)).generateAndSaveUpdateLogsForParent(Mockito.any(), Mockito.any());
        Mockito.verify(messageSender, Mockito.times(1)).sendMessage(Mockito.any());
    }

    @Test
    public void findFewByIsWorking() throws Exception {
        itemService.findFewByIsWorking(0, 10, true);
        Mockito.verify(itemDao, Mockito.times(1)).findFewByIsWorking(0, 10, true);
    }

    @Test
    public void getItemsCount() throws Exception {
        itemService.getItemsCount();
        Mockito.verify(itemDao, Mockito.times(1)).getRecordsCount();
    }

    @Test
    public void getItemsFilteredByIsWorkingCount() throws Exception {
        itemService.getItemsFilteredByIsWorkingCount(true);
        Mockito.verify(itemDao, Mockito.times(1)).getItemsFilteredByIsWorkingCount(true);
    }

    @Test
    public void assignItemToUser() throws Exception {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();

        PrivateItem privateItem = EntityFactory.createComplexPrivateItem();
        privateItem.setOwner(user);

        Mockito.when(itemService.assignItemToUser(complex, user.getId())).thenReturn(true);
        Mockito.when(privateItemService.findById(complex.getId())).thenReturn(privateItem);

        itemService.assignItemToUser(complex, user.getId());

        Mockito.verify(itemDao, Mockito.times(1)).assignItemToUser(complex, user.getId());
        Mockito.verify(messageSender, Mockito.times(complex.getComponents().size() + 1)).sendMessage(Mockito.any());
    }

    @Test
    public void assignItemToDepartment() throws Exception {
        Department department = EntityFactory.createEmptyDepartment();

        CommonItem commonItem = EntityFactory.createComplexCommonItem();
        commonItem.setDepartment(department);

        Mockito.when(itemService.assignItemToDepartment(complex, department.getId())).thenReturn(true);
        Mockito.when(commonItemService.findById(complex.getId())).thenReturn(commonItem);

        itemService.assignItemToDepartment(complex, department.getId());

        Mockito.verify(itemDao, Mockito.times(1)).assignItemToDepartment(complex, department.getId());
        Mockito.verify(messageSender, Mockito.times(complex.getComponents().size() + 1)).sendMessage(Mockito.any());
    }

}