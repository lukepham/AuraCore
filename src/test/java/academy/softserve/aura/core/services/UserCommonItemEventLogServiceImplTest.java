package academy.softserve.aura.core.services;

import academy.softserve.aura.core.dao.CommonItemDao;
import academy.softserve.aura.core.dao.UserDao;
import academy.softserve.aura.core.dao.impl.UserCommonItemEventDaoImpl;
import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserCommonItemEvent;
import academy.softserve.aura.core.exceptions.ItemNotFoundException;
import academy.softserve.aura.core.services.impl.UserCommonItemEventLogServiceImpl;
import academy.softserve.aura.core.services.messaging.KafkaMessageSender;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.OffsetDateTime;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserCommonItemEventLogServiceImplTest {

    @InjectMocks
    private UserCommonItemEventLogServiceImpl service;

    @Mock
    private UserCommonItemEventDaoImpl dao;

    @Mock
    private CommonItemDao commonItemDao;

    @Mock
    private UserDao userDao;

    @Mock
    KafkaMessageSender messageSender;

    private OffsetDateTime date = OffsetDateTime.now();
    private long userId = 1;
    private int offset = 0;
    private int limit = 10;

    @Test(expected = ItemNotFoundException.class)
    public void createWithNotExistingCommonItem() {
        UserCommonItemEvent entity = EntityFactory.createUserCommonItemEvent();
        Long itemId = 1L;

        entity.setCommonItemId(itemId);

        when(commonItemDao.getElementByID(itemId)).thenReturn(null);

        service.create(entity);
        Mockito.verify(dao, Mockito.times(1)).addElement(entity);
    }

    @Test
    public void create() {
        UserCommonItemEvent entity = EntityFactory.createUserCommonItemEvent();
        CommonItem item = EntityFactory.createNotComplexCommonItem();
        Long itemId = 1L;

        item.setId(itemId);
        entity.setCommonItemId(itemId);

        when(commonItemDao.getElementByID(itemId)).thenReturn(item);
        when(dao.addElement(entity)).thenReturn(entity);
        when(userDao.getElementByID(anyLong())).thenReturn(new User());

        service.create(entity);
        Mockito.verify(dao, Mockito.times(1)).addElement(entity);
    }

    @Test
    public void sendMessage(){
        UserCommonItemEvent entity = EntityFactory.createUserCommonItemEvent();
        CommonItem item = EntityFactory.createNotComplexCommonItem();
        Long itemId = 1L;

        item.setId(itemId);
        entity.setCommonItemId(itemId);

        when(commonItemDao.getElementByID(itemId)).thenReturn(item);
        when(dao.addElement(entity)).thenReturn(entity);

        service.create(entity);
        Mockito.verify(messageSender, Mockito.times(1)).sendMessage(entity);
    }

    @Test
    public void getAllSinceTime() {
        service.getAllSinceTime(date, offset, limit);
        Mockito.verify(dao, Mockito.times(1)).getAllSinceTime(date, offset, limit);
    }

    @Test
    public void getAllByUserId() {
        service.getAllByUserId(userId, offset, limit);
        Mockito.verify(dao, Mockito.times(1)).getAllByUserId(userId, offset, limit);
    }

    @Test
    public void getAllByUserIdAndTime() {
        service.getAllByUserIdAndTime(userId, date, offset, limit);
        Mockito.verify(dao, Mockito.times(1))
                .getAllByUserIdAndTime(userId, date, offset, limit);
    }

    @Test
    public void getAllSinceTimeCount() {
        service.getAllSinceTimeCount(date);
        Mockito.verify(dao, Mockito.times(1)).getAllSinceTimeCount(date);
    }

    @Test
    public void getAllByUserIdCount() {
        service.getAllByUserIdCount(userId);
        Mockito.verify(dao, Mockito.times(1)).getAllByUserIdCount(userId);
    }

    @Test
    public void getAllByUserIdAndTimeCount() {
        service.getAllByUserIdAndTimeCount(userId, date);
        Mockito.verify(dao, Mockito.times(1)).getAllByUserIdAndTimeCount(userId, date);
    }

}