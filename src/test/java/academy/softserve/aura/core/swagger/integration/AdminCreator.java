package academy.softserve.aura.core.swagger.integration;


import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.services.UserService;
import academy.softserve.aura.core.utils.EntityFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * Bean is required to check if default admin exists in database
 * in case there is no entity with login "superadmin",
 * will be persisted entity with login: "superadmin", password: "mysecretpassword", userRole: ADMIN
 */
public class AdminCreator implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminCreator.class);

    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        addAdminIfNotExists();
    }

    private void addAdminIfNotExists() {
        String login = "superadmin";
        String pass = "mysecretpassword";
        if (userService.findByLogin(login) == null) {
            academy.softserve.aura.core.entity.User superadmin = new academy.softserve.aura.core.entity.User();
            superadmin.setContacts(EntityFactory.createSimpleContacts());
            superadmin.setLogin(login);
            superadmin.setPassword(pass);
            superadmin.setUserRole(UserRole.ADMIN);
            superadmin.setFirstName("Super");
            superadmin.setLastName("Admin");
            superadmin.setActive(true);

            userService.create(superadmin);
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(String.format("Did not found \'superadmin\'\ncreating new ADMIN with login:\'%s\' and pass:\'%s\'", login, pass));
            }
        } else {
            LOGGER.info("\'superadmin\' exists, do not create new one");
        }
    }

}
