package academy.softserve.aura.core.swagger.controllers;

import academy.softserve.aura.core.entity.UserCommonItemEvent;
import academy.softserve.aura.core.mappers.DtoMapper;
import academy.softserve.aura.core.mappers.UserCommonItemEventMapper;
import academy.softserve.aura.core.services.UserCommonItemEventLogService;
import academy.softserve.aura.core.swagger.model.UserCommonItemEventDto;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserCommonItemEventLogApiControllerTest {

    @InjectMocks
    private UserCommonItemEventLogApiController controller;
    @Mock
    private DtoMapper<UserCommonItemEventDto, UserCommonItemEvent> mapper = new UserCommonItemEventMapper();
    @Mock
    private UserCommonItemEventLogService service;

    private HttpHeaders headers = new HttpHeaders();
    private UserCommonItemEvent entity = EntityFactory.createUserCommonItemEvent();
    private UserCommonItemEventDto dto = mapper.toDto(entity);
    private List<UserCommonItemEventDto> dtoList;
    private List<UserCommonItemEvent> entityList;
    private OffsetDateTime date = OffsetDateTime.now();
    private long userId = 1;
    private int offset = 0;
    private int limit = 10;

    @Before
    public void setUp() {
        UserCommonItemEvent event = EntityFactory.createUserCommonItemEvent();

        entityList = new ArrayList<>();
        dtoList = new ArrayList<>();

        entityList.add(event);
        dtoList.add(mapper.toDto(event));

        headers.add("X-total-records-count", "1");

        when(service.getRecordsCount()).thenReturn(1L);
        when(service.getAllByUserIdCount(userId)).thenReturn(1L);
        when(service.getAllSinceTimeCount(date)).thenReturn(1L);
        when(service.getAllByUserIdAndTimeCount(userId, date)).thenReturn(1L);
    }

    @Test
    public void addExistingUserCommonItemEvent() {
        when(service.create(entity)).thenReturn(entity);
        when(mapper.toEntity(dto)).thenReturn(entity);
        when(mapper.toDto(entity)).thenReturn(dto);

        assertEquals(new ResponseEntity<>(dto, HttpStatus.OK), controller.addUserCommonItemEvent(dto));
        Mockito.verify(service, Mockito.times(1)).create(entity);
    }

    @Test
    public void addNotExistingUserCommonItemEvent() {
        UserCommonItemEventDto eventDto = Mockito.mock(UserCommonItemEventDto.class);

        when(service.create(entity)).thenReturn(entity);
        when(mapper.toDto(entity)).thenReturn(eventDto);
        when(mapper.toEntity(eventDto)).thenReturn(entity);
        when(eventDto.getId()).thenReturn(null);

        assertEquals(new ResponseEntity<>(eventDto, HttpStatus.CREATED),
                controller.addUserCommonItemEvent(eventDto));
        Mockito.verify(service, Mockito.times(1)).create(entity);
    }

    @Test
    public void getCommonItemLogsWithAllParameters() {
        when(service.getAllByUserIdAndTime(userId, date, offset, limit)).thenReturn(entityList);

        assertEquals(new ResponseEntity<>(dtoList, headers, HttpStatus.OK),
                controller.getCommonItemLogs(offset, limit, userId, date));
        Mockito.verify(service,
                Mockito.times(1)).getAllByUserIdAndTime(userId, date, offset, limit);
    }

    @Test
    public void getCommonItemLogsWithUser() {
        when(service.getAllByUserId(userId, offset, limit)).thenReturn(entityList);

        assertEquals(new ResponseEntity<>(dtoList, headers, HttpStatus.OK),
                controller.getCommonItemLogs(offset, limit, userId, null));
        Mockito.verify(service,
                Mockito.times(1)).getAllByUserId(userId, offset, limit);
    }

    @Test
    public void getCommonItemLogsWithDate() {
        when(service.getAllSinceTime(date, offset, limit)).thenReturn(entityList);

        assertEquals(new ResponseEntity<>(dtoList, headers, HttpStatus.OK),
                controller.getCommonItemLogs(offset, limit, null, date));
        Mockito.verify(service,
                Mockito.times(1)).getAllSinceTime(date, offset, limit);
    }

    @Test
    public void getCommonItemLogsWithNoParameters() {
        when(service.findFew(offset, limit)).thenReturn(entityList);

        assertEquals(new ResponseEntity<>(dtoList, headers, HttpStatus.OK),
                controller.getCommonItemLogs(offset, limit, null, null));
        Mockito.verify(service,
                Mockito.times(1)).findFew(offset, limit);
    }

    @Test
    public void getCommonItemLogsWithNullResult() {
        when(service.getAllByUserIdAndTime(userId, date, offset, limit)).thenReturn(null);

        assertEquals(new ResponseEntity<>(new ArrayList<>(), headers, HttpStatus.NOT_FOUND),
                controller.getCommonItemLogs(offset, limit, userId, date));
        Mockito.verify(service,
                Mockito.times(1)).getAllByUserIdAndTime(userId, date, offset, limit);
    }

    @Test
    public void getCommonItemLogsWithEmptyResult() {
        when(service.getAllByUserIdAndTime(userId, date, offset, limit)).thenReturn(new ArrayList<>());

        assertEquals(new ResponseEntity<>(new ArrayList<>(), headers, HttpStatus.NOT_FOUND),
                controller.getCommonItemLogs(offset, limit, userId, date));
        Mockito.verify(service,
                Mockito.times(1)).getAllByUserIdAndTime(userId, date, offset, limit);
    }

}