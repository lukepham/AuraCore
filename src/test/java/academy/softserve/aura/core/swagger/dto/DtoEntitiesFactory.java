package academy.softserve.aura.core.swagger.dto;


import academy.softserve.aura.core.swagger.model.*;

import java.math.BigDecimal;
import java.util.ArrayList;

import static academy.softserve.aura.core.utils.EntityFactory.*;


public class DtoEntitiesFactory {


    public static ContactsDto createContactsDto() {
        ContactsDto contacts = new ContactsDto();

        PhoneDto e = new PhoneDto();
        e.setPhoneNumber("+380973991848");

        EmailDto m = new EmailDto();
        m.setEmailAddress("email@email");

        AddressDto a = new AddressDto();
        a.setFullAddress("Kyiv");

        contacts.setPhones(new ArrayList<PhoneDto>() {{
            add(e);
        }});
        contacts.setAddresses(new ArrayList<AddressDto>() {{
            add(a);
        }});
        contacts.setEmails(new ArrayList<EmailDto>() {{
            add(m);
        }});
        return contacts;
    }

    public static UserDto createUserDto() {
        UserDto userDto = new UserDto();
        userDto.setContacts(createContactsDto());
        userDto.setFirstName("First Name");
        userDto.setLastName("Last Name");
        userDto.setIsActive(true);
        return userDto;
    }

    public static DepartmentDto createEmptyDepartment() {
        DepartmentDto departmentDto = new DepartmentDto();
        departmentDto.setContacts(createContactsDto());
        departmentDto.setName("Department");

        return departmentDto;
    }

    public static UserWithCredentialsDto createUserWithCredentialsDto() {
        UserWithCredentialsDto userWithCredentialsDto = new UserWithCredentialsDto();
        userWithCredentialsDto.setContacts(createContactsDto());
        userWithCredentialsDto.setFirstName("First Name");
        userWithCredentialsDto.setLastName("Last Name");
        userWithCredentialsDto.setIsActive(true);
        userWithCredentialsDto.setLogin("login" + System.currentTimeMillis());
        userWithCredentialsDto.setPassword("pass");
        userWithCredentialsDto.setRole(Enum.valueOf(UserWithCredentialsDto.RoleEnum.class, "USER"));
        return userWithCredentialsDto;
    }

    public static CommonItemDto createCommonItem(DepartmentDto departmentDto) {
        return null;
    }

    public static ItemDto createNotComplexItemDto() {
        ItemDto itemDto = new ItemDto();
        itemDto.setIsWorking(IS_ITEM_WORKING);
        itemDto.setManufacturer(ITEM_MANUFACTURER);
        itemDto.setModel(ITEM_MODEL);
        itemDto.setStartPrice(ITEM_START_PRICE);
        itemDto.setNominalResource(ITEM_NOMINAL_RESOURCE);
        itemDto.setId((long) (Math.random() * 100));
        itemDto.setDescription(ITEM_DESCRIPTION);
        itemDto.setCurrentPrice(new BigDecimal(ITEM_CURRENT_PRICE));
        itemDto.setComponents(new ArrayList<>());
        itemDto.setManufactureDate(ITEM_USAGE_START_DATE);
        itemDto.setAgingFactor(AGING_FACTOR);
        itemDto.setItemType("Item");
        return itemDto;
    }

    public static ItemDto createComplexItemDto() {
        ItemDto parent = createNotComplexItemDto();
        ItemDto comp1 = createNotComplexItemDto();
        ItemDto comp2 = createNotComplexItemDto();
        parent.getComponents().add(comp1);
        parent.getComponents().add(comp2);

        return parent;
    }

    public static PrivateItemDto createNotComplexPrivateItemDto() {
        PrivateItemDto itemDto = new PrivateItemDto();
        itemDto.setUserId(1L);
        itemDto.setIsWorking(IS_ITEM_WORKING);
        itemDto.setManufacturer(ITEM_MANUFACTURER);
        itemDto.setModel(ITEM_MODEL);
        itemDto.setStartPrice(ITEM_START_PRICE);
        itemDto.setNominalResource(ITEM_NOMINAL_RESOURCE);
        itemDto.setId((long) (Math.random() * 100));
        itemDto.setDescription(ITEM_DESCRIPTION);
        itemDto.setCurrentPrice(new BigDecimal(ITEM_CURRENT_PRICE));
        itemDto.setComponents(new ArrayList<>());
        itemDto.setUsageStartDate(ITEM_USAGE_START_DATE);
        itemDto.setManufactureDate(ITEM_USAGE_START_DATE);
        itemDto.setAgingFactor(AGING_FACTOR);
        itemDto.setItemType("PrivateItem");
        return itemDto;
    }

    public static PrivateItemDto createComplexPrivateItemDto() {
        PrivateItemDto parent = createNotComplexPrivateItemDto();
        PrivateItemDto  comp1 = createNotComplexPrivateItemDto();
        PrivateItemDto  comp2 = createNotComplexPrivateItemDto();
        parent.getComponents().add(comp1);
        parent.getComponents().add(comp2);

        return parent;
    }

    public static CommonItemDto createNotComplexCommonItemDto() {
        CommonItemDto itemDto = new CommonItemDto();
        itemDto.setDepartmentId(1L);
        itemDto.setIsWorking(IS_ITEM_WORKING);
        itemDto.setManufacturer(ITEM_MANUFACTURER);
        itemDto.setModel(ITEM_MODEL);
        itemDto.setStartPrice(ITEM_START_PRICE);
        itemDto.setNominalResource(ITEM_NOMINAL_RESOURCE);
        itemDto.setId((long) (Math.random() * 100));
        itemDto.setDescription(ITEM_DESCRIPTION);
        itemDto.setCurrentPrice(new BigDecimal(ITEM_CURRENT_PRICE));
        itemDto.setComponents(new ArrayList<>());
        itemDto.setManufactureDate(ITEM_USAGE_START_DATE);
        itemDto.setAgingFactor(AGING_FACTOR);
        itemDto.setItemType("CommonItem");
        return itemDto;
    }

    public static CommonItemDto createComplexCommonItemDto() {
        CommonItemDto parent = createNotComplexCommonItemDto();
        CommonItemDto  comp1 = createNotComplexCommonItemDto();
        CommonItemDto  comp2 = createNotComplexCommonItemDto();
        parent.getComponents().add(comp1);
        parent.getComponents().add(comp2);

        return parent;
    }

}

