package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.exceptions.AuraMapperIllegalMethodException;
import academy.softserve.aura.core.swagger.model.UserDto;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class UserMapperTest {

    @Autowired
    private DtoMapper<UserDto, User> userMapper;

    @Test
    public void shouldMapUserToDto() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();

        UserDto userDto = userMapper.toDto(user);
        assertEquals(user.getId(), userDto.getId());
        assertEquals(user.getFirstName(), userDto.getFirstName());
        assertEquals(user.getLastName(), userDto.getLastName());
        assertEquals(user.isActive(), userDto.getIsActive());
        assertNull(userMapper.toDto(null));

        user.getDepartment().setId(null);
        userDto = userMapper.toDto(user);
        assertNull(userDto.getDepartmentId());

        user.setDepartment(null);
        userDto = userMapper.toDto(user);
        assertNull(userDto.getDepartmentId());

    }

    @Test(expected = AuraMapperIllegalMethodException.class)
    public void shouldMapUserDtoToUser() {
        UserDto userDto = new UserDto();
        User user = userMapper.toEntity(userDto);
    }

}
