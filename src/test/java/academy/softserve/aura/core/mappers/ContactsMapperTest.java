package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Address;
import academy.softserve.aura.core.entity.Contacts;
import academy.softserve.aura.core.entity.Email;
import academy.softserve.aura.core.entity.Phone;
import academy.softserve.aura.core.swagger.model.AddressDto;
import academy.softserve.aura.core.swagger.model.ContactsDto;
import academy.softserve.aura.core.swagger.model.EmailDto;
import academy.softserve.aura.core.swagger.model.PhoneDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class ContactsMapperTest {
    @Autowired
    DtoMapper<ContactsDto, Contacts> mapper;

    private Contacts contacts = new Contacts();
    private ContactsDto contactsDto = new ContactsDto();
    private Email email = new Email();
    private EmailDto emailDto = new EmailDto();
    private Phone phone = new Phone();
    private PhoneDto phoneDto = new PhoneDto();
    private Address address = new Address();
    private AddressDto addressDto = new AddressDto();

    @Before
    public void makeStubs() {

        contacts.setEmails(new ArrayList<>());
        contacts.setPhones(new ArrayList<>());
        contacts.setAddresses(new ArrayList<>());

        email.setId(1L);
        email.setEmailAddress("test@toDto");
        emailDto.setEmailAddress("test@toEntity");
        emailDto.setId(3L);

        phone.setId(1L);
        phone.setPhoneNumber("phone_toDto");
        phoneDto.setPhoneNumber("phone_toEntity");
        phoneDto.setId(3L);

        address.setId(1L);
        address.setFullAddress("test_address1");
        addressDto.setFullAddress("test_address2");
        addressDto.setId(3L);

        contacts.setId(1L);
        contacts.getEmails().add(email);
        contacts.getPhones().add(phone);
        contacts.getAddresses().add(address);
        contactsDto.setId(3L);
        contactsDto.getEmails().add(emailDto);
        contactsDto.getPhones().add(phoneDto);
        contactsDto.getAddresses().add(addressDto);


    }

    @Test
    public void toDto() throws Exception {
        contactsDto = mapper.toDto(contacts);
        assertEquals(email.getId(), contactsDto.getEmails().get(0).getId());
        assertEquals(email.getEmailAddress(), contactsDto.getEmails().get(0).getEmailAddress());
        assertEquals(phone.getId(), contactsDto.getEmails().get(0).getId());
        assertEquals(phone.getPhoneNumber(), contactsDto.getPhones().get(0).getPhoneNumber());
        assertEquals(address.getId(), contactsDto.getEmails().get(0).getId());
        assertEquals(address.getFullAddress(), contactsDto.getAddresses().get(0).getFullAddress());
        assertNull(mapper.toDto(null));

    }

    @Test
    public void toEntity() throws Exception {
        contacts = mapper.toEntity(contactsDto);

        assertEquals(emailDto.getId(), contacts.getEmails().get(0).getId());
        assertEquals(emailDto.getEmailAddress(), contacts.getEmails().get(0).getEmailAddress());
        assertEquals(phoneDto.getId(), contacts.getEmails().get(0).getId());
        assertEquals(phoneDto.getPhoneNumber(), contacts.getPhones().get(0).getPhoneNumber());
        assertEquals(addressDto.getId(), contacts.getEmails().get(0).getId());
        assertEquals(addressDto.getFullAddress(), contacts.getAddresses().get(0).getFullAddress());
        assertNull(mapper.toEntity(null));

        contactsDto.setId(null);
        contacts = mapper.toEntity(contactsDto);

        assertNull(contacts.getId());

    }

}