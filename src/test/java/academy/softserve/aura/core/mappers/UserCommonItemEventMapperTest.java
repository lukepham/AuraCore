package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserCommonItemEvent;
import academy.softserve.aura.core.swagger.model.UserCommonItemEventDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.OffsetDateTime;

import static org.junit.Assert.*;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class UserCommonItemEventMapperTest {
    @Autowired
    UserCommonItemEventMapper mapper;
    UserCommonItemEvent entity;
    UserCommonItemEventDto dto;
    CommonItem cI;
    User u;

    @Before
    public void createStubs(){
        cI = new CommonItem();
        u = new User();
        u.setId(1L);
        cI.setId(1L);
        entity = new UserCommonItemEvent();
        entity.setId(1L);
        entity.setCommonItemId(cI.getId());
        entity.setUsageDuration(1.1);
        entity.setUsageStartDate(OffsetDateTime.now());
        entity.setUsageEndDate(OffsetDateTime.now());
        entity.setUser(u);
    }

    @Test
    public void toDto() {
        dto = mapper.toDto(entity);
        assertEquals(dto.getId(), entity.getId());
        assertEquals(dto.getUserId(), entity.getUser().getId());
        assertEquals(dto.getCommonItemId(), entity.getCommonItemId());
        assertEquals(dto.getUsageDuration(), (Double) entity.getUsageDuration());
        assertEquals(dto.getUsageStartDate(), entity.getUsageStartDate());
        assertEquals(dto.getUsageEndDate(), entity.getUsageEndDate());

        entity.setCommonItemId(null);
        assertNull(mapper.toDto(entity).getCommonItemId());

        u.setId(null);
        assertNull(mapper.toDto(entity).getUserId());

        entity.setUser(null);
        assertNull(mapper.toDto(entity).getUserId());
        assertNull(mapper.toDto(null));

    }

    @Test
    public void toEntity() {
        dto = mapper.toDto(entity);
        UserCommonItemEvent event = mapper.toEntity(dto);

        assertEquals(event.getId(), dto.getId());
        assertEquals(event.getUser().getId(), dto.getUserId());
        assertEquals(event.getCommonItemId(), dto.getCommonItemId());
        assertNotEquals(event.getUsageDuration(), dto.getUsageDuration());
        assertEquals(event.getUsageStartDate(), dto.getUsageStartDate());
        assertEquals(event.getUsageEndDate(), dto.getUsageEndDate());
    }

}