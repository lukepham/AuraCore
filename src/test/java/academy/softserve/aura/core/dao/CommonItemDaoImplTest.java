package academy.softserve.aura.core.dao;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:daos/test_dao_application_context.xml")
public class CommonItemDaoImplTest {


    @Autowired
    DepartmentDao departmentDao;

    @Autowired
    CommonItemDao commonItemDao;

    private List<Long> departmentIds = null;

    private int numberOfItems;
    private Department department1;
    private Department department2;
    private Department department3;

    private CommonItem commonItem1;
    private CommonItem commonItem2;
    private CommonItem commonItem3;

    @Before
    public void setUp() throws Exception {
        department1 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department1);
        department2 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department2);
        department3 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department3);

        commonItem1 = EntityFactory.createNotComplexCommonItem();
        commonItem1.setModel("item1");
        commonItem2 = EntityFactory.createNotComplexCommonItem();
        commonItem2.setModel("item2");
        commonItem3 = EntityFactory.createNotComplexCommonItem();
        commonItem3.setModel("item3");

        commonItem1.setDepartment(department1);
        commonItem2.setDepartment(department2);
        commonItem3.setDepartment(department3);

        commonItem1.setWorking(true);
        commonItem2.setWorking(true);
        commonItem3.setWorking(false);

        commonItemDao.addElement(commonItem1);
        commonItemDao.addElement(commonItem2);
        commonItemDao.addElement(commonItem3);

        numberOfItems = commonItemDao.getAllElements().size();

        departmentIds = new ArrayList<>();
        departmentIds.add(department1.getId());
        departmentIds.add(department2.getId());
    }

    @Test
    @Transactional
    public void getCommonItemsFilteredByIsWorkingCount() throws Exception {
        long notWorkingItems = commonItemDao.getCommonItemsFilteredByIsWorkingCount(false);
        long workingItems = commonItemDao.getCommonItemsFilteredByIsWorkingCount(true);
        Assert.assertNotNull(notWorkingItems);
        Assert.assertNotNull(workingItems);
        Assert.assertEquals(1L, notWorkingItems);
        Assert.assertEquals(2L, workingItems);
    }

    @Test
    @Transactional
    public void assignCommonItemToWarehouse() throws Exception {
        boolean result = commonItemDao.assignCommonItemToWarehouse(commonItem1);
        Assert.assertTrue(result);
    }

    @Test
    @Transactional
    public void findFewByIsWorking() throws Exception {
        List<CommonItem> commonItems = (List<CommonItem>) commonItemDao.findFewByIsWorking(0,2,true);
        commonItems.stream().forEach(commonItem -> System.out.println(commonItem.isWorking()));
        assertEquals(2,commonItems.size());
    }

    @Test
    @Transactional
    public void getCommonItemDepartments() throws Exception {
        //from log
    }

    @Test
    @Transactional
    public void addElement() throws Exception {
        CommonItem commonItem1 = EntityFactory.createNotComplexCommonItem();
        commonItem1.setModel("Test item");
        commonItem1.setDepartment(department1);

        commonItemDao.addElement(commonItem1);

        List<CommonItem> commonItems = (List<CommonItem>) commonItemDao.getAllElements();
        assertEquals("Test item",commonItems.get(numberOfItems).getModel());
    }

    @Test
    @Transactional
    public void addAll() throws Exception {
        CommonItem commonItem1 = EntityFactory.createNotComplexCommonItem();
        commonItem1.setDepartment(department1);
        CommonItem commonItem2 = EntityFactory.createNotComplexCommonItem();
        commonItem2.setDepartment(department1);

        List<CommonItem> commonItems = new ArrayList<>();
        commonItems.add(commonItem1);
        commonItems.add(commonItem2);

        commonItemDao.addAll(commonItems);
        List<CommonItem> itemsFrom = (List<CommonItem>) commonItemDao.getAllElements();
        assertEquals(commonItems.size()+numberOfItems,itemsFrom.size());
    }

    @Test
    @Transactional
    public void updateElement() throws Exception {
        commonItem1.setModel("updated item");
        commonItemDao.updateElement(commonItem1);
        assertEquals("updated item",commonItemDao.getElementByID(commonItem1.getId()).getModel());
    }

    @Test
    @Transactional
    public void getElementByID() throws Exception {
        CommonItem test = commonItemDao.getElementByID(commonItem1.getId());
        assertNotNull(test);
    }

    @Test
    @Transactional
    public void getAllElements() throws Exception {
        int number = commonItemDao.getAllElements().size();
        assertEquals(numberOfItems,number);
    }

    @Test
    @Transactional
    public void deleteElement() throws Exception {
        commonItemDao.deleteElement(commonItem1.getId());
        assertNull(commonItemDao.getElementByID(commonItem1.getId()));
    }

    @Test
    @Transactional
    public void getFew() throws Exception {
        int number = commonItemDao.getFew(0,2).size();
        assertEquals(2,number);
    }

}