FROM bitnami/tomcat:8.5

RUN install_packages postgresql-client

# Deploy app
COPY target/AuraCore-*.war /bitnami/tomcat/data/ROOT.war

COPY rootfs /

# Setup Liquibase
RUN curl -sSL https://github.com/liquibase/liquibase/releases/download/liquibase-parent-3.5.3/liquibase-3.5.3-bin.tar.gz > /liquibase.tar.gz && \
mkdir /liquibase && \
tar -xzf liquibase.tar.gz -C /liquibase && \
curl -sSL https://jdbc.postgresql.org/download/postgresql-42.1.1.jar > /liquibase/postgresql.jar

COPY liquibase/changelogs /liquibase

COPY liquibase/liquibase.properties /liquibase